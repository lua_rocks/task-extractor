-- 08-06-2024 @author Swarg
local M = {}

local gumbo = require "gumbo"
local inspect = require "inspect"
local base = require 'cli-app-base'

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local validate_input_n_output_filenames = base.validate_input_n_output_filenames

local DEF_REMOVED_ELM_BY_ATTRS = {
  script = {
    crossorigin = "anonymous",
    src = nil, -- by site
  },
  meta = {
    name = {
      ["request-id"] = 1,
      ["html-safe-nonce"] = 1,
      ["visitor-payload"] = 1,
      ["visitor-hmac"] = 1,
      ["browser-stats-url"] = 1,
      ["browser-errors-url"] = 1,
      ["theme-color"] = 1,
      ["color-scheme"] = 1,
      ["turbo-cache-control"] = 1,
    },
    -- ['data-pjax-transient'] = "true"
  },
  link = {
    crossorigin = "anonymous",
    rel = {
      manifest = 1,
      preconnect = 1,
      ['dns-prefetch'] = 1,
      icon = 1,
      ['mask-icon'] = 1,
      ['alternate icon'] = 1,
    }
  },
  a = {
    href = '#start-of-content',
    ['data-hydro-click'] = true, -- any value
    ['data-analytics-event'] = true,
    ['data-ga-click'] = true,
    ['data-view-component'] = true, --?
  },
  div = {
    ['data-target'] = "react-partial.reactRoot"
  }

}

-- filter by elemnt attrs
local function is_elm_to_remove_by_attr(tattr, map, tag)
  local exp_value = map[tattr.name or false]
  local f = exp_value == true or exp_value == tattr.value or
      -- list of values to remove
      type(exp_value) == 'table' and exp_value[tattr.value or false] == 1
  if tag == 'meta' then
    print('META remove:', f, tattr.name, '=', tattr.value, 'exp:', inspect(exp_value))
  end
  return f
end

function M.trim(s)
  return (s:gsub("^%s*(.-)%s*$", "%1"))
end

-- removing "unnecessary" elements of the DOM to reduce file size as much as
-- possible
---@param fn string
---@param outfn string?
---@param force_rewrite boolean?
---@param tags table -- list of tags
---@param attr_map table -- map with tags-attrs to remove
---@param verbose boolean?
---@return boolean, string|table
function M.slim(fn, outfn, force_rewrite, tags, attr_map, verbose)
  local ok, ret = validate_input_n_output_filenames(fn, outfn, force_rewrite)
  if not ok then return false, ret end -- errmsg
  outfn = ret
  tags = tags or {
    'style', 'svg', 'script', 'link', 'meta', 'template',
    'a', 'ul', 'button',
    'div',
    --github specific?
    'qbsearch-input',
    'dialog-helper', -- some token authenticity_token"
  }
  -- "tags" elements with given attributes what can be deleted
  attr_map = attr_map or DEF_REMOVED_ELM_BY_ATTRS
  local c, ca, cak = 0, 0, 0

  local document = assert(gumbo.parseFile(fn))

  for _, tag in ipairs(tags) do
    local elms = assert(document:getElementsByTagName(tag))
    local map = attr_map[tag]
    local has_map = type(map) == 'table' and next(map) ~= nil

    for _, elm in ipairs(elms) do
      local can_remove = true
      if has_map and type(elm.attributes) == 'table' then
        can_remove = false
        for _, tattr in pairs(elm.attributes) do
          if type(tattr) == 'table' then
            if is_elm_to_remove_by_attr(tattr, map, tag) then
              can_remove = true
              ca = ca + 1
              break
            end
          end
        end

        if not can_remove then
          if verbose then
            local attrs = ''
            for _, tattr in pairs(elm.attributes) do
              if attrs ~= '' then
                attrs = attrs .. ', '
              end
              if type(tattr) == 'table' then
                attrs = attrs .. v2s(tattr.name) .. '=' .. v2s(tattr.value)
              else
                attrs = v2s(tattr) -- number?
              end
            end
            print('Keep by attr elm:', elm.localName, 'attrs:', attrs)
          end
          cak = cak + 1
        end
      end
      if can_remove then
        -- print('remove', element.localName)
        elm:remove()
        c = c + 1
      end
    end
  end

  local html = document:serialize() --f2)

  local f2, err2 = io.open(outfn, 'wb')
  if not f2 then
    return false, 'cannot open file to write ' .. v2s(outfn) .. v2s(err2)
  end

  local s, elc, sc = '', 0, 0
  -- remove empty lines
  for line in string.gmatch(html or '', '([^\n]+)') do
    local before = #line
    line = M.trim(line)
    local spaces = before - #line
    sc = sc + spaces
    if line ~= '' then -- trim?
      s = s .. line .. "\n"
    else
      elc = elc + 1
    end
  end

  f2:write(s)
  f2:close()

  local st = {
    output = outfn,
    elm_removed = c,
    elm_removed_by_attr = ca,
    elm_keeped_by_attr = cak,
    removed_empty_lines = elc,
    removed_spaces = sc,
  }
  return true, st
end

return M
