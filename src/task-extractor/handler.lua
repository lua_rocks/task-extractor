-- 06-06-2024 @author Swarg
-- App Entry Point
local log = require 'alogger'
local Cmd4Lua = require 'cmd4lua'

local builtin = require "cli-app-base.builtin-cmds"

local cache = require "task-extractor.cmd.cache"
local html = require "task-extractor.cmd.html"
local json = require "task-extractor.cmd.json"
local github = require "task-extractor.cmd.github"
local npmjs = require "task-extractor.cmd.npmjs"
local eclipse = require "task-extractor.cmd.eclipse"
local sql_academy = require "task-extractor.cmd.sql_academy"
local directory = require "task-extractor.cmd.directory"
local sources = require "task-extractor.cmd.sources"


local M = {}
M._VERSION = 'task-extractor v0.6.1'
M._URL = 'https://gitlab.com/lua_rocks/task-extractor'


---@param args string|table
function M.handle(args)
  log.debug('handle', args)
  return
      Cmd4Lua.of(args)
      :root('task-extractor')
      :about('The ToolSet for extract task from different sites')
      :handlers(M)

      :desc('version of this application')
      :cmd('version', 'v')

      :desc('built-in commands of the appliction, such as config, logger')
      :cmd('builtin', 'b', builtin.handle)

      :desc('built-in commands of the appliction, such as config, logger')
      :cmd('cache', 'c', cache.handle)
      -- api

      :desc('sql-academy.org')
      :cmd('github', 'gh', github.handle)

      :desc('npmjs.com nodejs repository')
      :cmd('npmjs', 'np', npmjs.handle)

      :desc('download stuff from eclipse.org')
      :cmd('eclipse', 'e', eclipse.handle)

      :desc('sql-academy.org')
      :cmd('sql-academy', 'sa', sql_academy.handle)

      :desc('tools to interact with html files')
      :cmd('html', 'h', html.handle)

      :desc('tools to interact with json files')
      :cmd('json', 'j', json.handle)

      :desc('create and compare snapshot of files in the directory')
      :cmd('directory', 'dir', directory.handle)

      :desc('interact with sources files on the local machine (count LoC)')
      :cmd('sources', 's', sources.handle)

      :run()
      :exitcode()
end

--
-- version of this application
--
function M.cmd_version()
  io.stdout:write(M._VERSION .. "\n")
end

return M
