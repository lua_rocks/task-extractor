-- 03-09-2024 @author Swarg
-- nodejs repository

local api = require 'task-extractor.api.npmjs'

--------------------------------------------------------------------------------

local M = {}

---@param w Cmd4Lua
function M.handle(w)
  w:about('interact with nodejs.com')
      :handlers(M)

      :desc('show info about given npm package')
      :cmd('alive-info', 'ai')

      :desc('build comparison table for list of given packages')
      :cmd('comparison-table', 'ct')

      :run()
end

--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

--
-- show info about given npm package
--
---@param w Cmd4Lua
function M.cmd_alive_info(w)
  w:v_opt_verbose('-v')
  w:v_opt_dry_run('-d')
  local pkgname = w:desc('full package name'):pop():arg()
  w:desc('to return raw json'):tag('raw'):v_has_opt('--raw-json', '-j')

  w:desc('show full numbers without abbreviations')
      :v_has_opt('--full-numbers', '-n')

  w:desc('with repository info (gihub, gitlab, etc)')
      :v_has_opt('--repo-info', '-r')

  if not w:is_input_valid() then return end

  local t, errmsg = api.alive_info(pkgname, w.vars)
  if not t then
    return w:error(v2s(errmsg))
  end

  print(api.fmt_alive_info(t, w.vars))
end

--
-- build comparison table for list of given packages
-- https://npmtrends.com/
--
---@param w Cmd4Lua
function M.cmd_comparison_table(w)
  w:usage('tet nodejs comparison-table [react vue svelte @svelte/kit]')

  local list = w:desc('package names'):pop():argl()
  w:desc('output format one of [markdown, html]')
      :def('markdown'):v_opt('--format', '-f')

  w:desc('minify cell size (no space between columns)')
      :v_has_opt('--minify', '-m')

  w:desc('do not add clarifying footnotes to the table')
      :v_has_opt('--no-footnotes', '-F')

  if not w:is_input_valid() then return end

  if #list == 0 then
    return w:error('expected not empty list of npm package names')
  end
  w.vars.notify = print
  local t = api.comparison_table(list, w.vars)
  return print(api.fmt_comparision_table(t, w.vars))
end

return M
