-- 17-06-2024 @author Swarg
-- Goals:
-- - create a snapshot of files in a directory
-- - comparison of snapshots to figureout what has changed

local base = require "cli-app-base"
local uv_ok, uv = pcall(require, "luv")

--------------------------------------------------------------------------------

local M = {}
---@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)
      :about('Create and compare snapshots of directory state')

      :desc('create a snapshot of files in given directory')
      :cmd('snapshot', 'sn')

      :run()
end

--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

--
-- create a snapshot of files in given directory
--
---@param w Cmd4Lua
function M.cmd_snapshot(w)
  local dir = w:desc('directory'):pop():arg()
  local output = w:desc('save to given file'):opt('--output', '-o')
  local detailed = w:desc('with size, date [crc32?]'):has_opt('--detailed', '-d')
  -- local md5 = w:desc('with size and date'):opt('--detailed', '-d')

  if not w:is_input_valid() then return end

  if dir == '.' then dir = os.getenv('PWD') end

  local files = base.list_of_files_recur(dir)
  local s = ''
  if detailed then
    if not uv_ok then return w:error('no luv package') end
    for _, fn in ipairs(files) do
      local st = uv.fs_stat(fn)
      local sz, mtime
      if type(st) == 'table' then
        sz, mtime = st.size, (st.mtime or E).sec
      end
      s = s .. fn .. ' ' .. v2s(sz) .. ' ' .. v2s(mtime) .. "\n"
    end
    --
  else
    s = table.concat(files, "\n")
  end

  if output then
    print(base.write(output, s, 'w'), output)
    return
  end
  print(s)
end

return M
