-- 06-06-2024 @author Swarg
local cache = require 'task-extractor.cache'

--------------------------------------------------------------------------------

local M = {}
---@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)
      :about('App cache to store temp data')

      :desc('status of all cached data')
      :cmd('status', 'st')

      :desc('put string data into cache with given key')
      :cmd('put', 'p')

      :desc('get string data into cache for given key')
      :cmd('get', 'g')

      :desc('clean one cached entry')
      :cmd('remove', 'rm')

      :desc('clean all cached data')
      :cmd('clear', 'clear')

      :run()
end

--------------------------------------------------------------------------------


--
-- status of all cached data
--
---@param w Cmd4Lua
function M.cmd_status(w)
  if not w:is_input_valid() then return end

  w:say(cache.status())
end

--
-- put string data into cache with given key
--
---@param w Cmd4Lua
function M.cmd_put(w)
  local key = w:desc('key'):pop():arg()
  local data = w:desc('data'):pop():arg()

  if not w:is_input_valid() then return end

  local cached = cache.put(true, key, data)
  w:say('cached:', key, cached)
end

--
-- get string data into cache for given key
--
---@param w Cmd4Lua
function M.cmd_get(w)
  local key = w:desc('key'):pop():arg()

  if not w:is_input_valid() then return end

  local data = cache.find(true, key)
  w:say(key, data)
end

--
-- clean one cached entry
--
---@param w Cmd4Lua
function M.cmd_remove(w)
  local key = w:desc('key'):pop():arg()

  if not w:is_input_valid() then return end

  local removed = cache.remove(key)
  w:say('removed:', key, removed)
end

--
-- clean all cached data
--
---@param w Cmd4Lua
function M.cmd_clear(w)
  if not w:is_input_valid() then return end
  w:say(cache.clear())
end

return M
