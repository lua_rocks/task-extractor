-- 27-06-2024 @author Swarg
--
local api = require 'task-extractor.api.eclipse.jdtls'

--------------------------------------------------------------------------------

local M = {}

---@param w Cmd4Lua
function M.handle(w)
  w:about('interact with eclipse.org')
      :handlers(M)

      :desc('Java language server')
      :cmd('jdtls', 'j')

      :run()
end

--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


--------------------------------------------------------------------------------
--                             Jdtls
--------------------------------------------------------------------------------

--
-- Java language server
--
---@param w Cmd4Lua
function M.cmd_jdtls(w)
  w:about('interact with download.eclipse.org/jdtls/milestones/')
      :handlers(M)

      :desc('show currently installed version')
      :cmd('version', 'v')

      :desc('List of all available versions of jdtls')
      :cmd('list', 'ls')

      :desc('download given version of jdtls to current dir')
      :cmd('download', 'd')

      :desc('install jdt-ls from tar.gz into ~/.local/share/jdtls ')
      :cmd('install', 'i')

      :desc("create symlink to fernflower decompiler used in jdtls")
      :cmd('link-decompiler-engine', 'lde')

      :run()
end

--
-- show currently installed version
--
---@param w Cmd4Lua
function M.cmd_version(w)
  local all = w:desc('show all used jars'):has_opt('--all', '-a')

  if not w:is_input_valid() then return end

  w:say(api.get_installed_version(all))
end

--
-- List of all jdtls versions available for download
--
---@param w Cmd4Lua
function M.cmd_list(w)
  if not w:is_input_valid() then return end
  local ok, t = api.fetch_page_with_milestones_versions(w.vars)
  if not ok then return w:error(v2s(t)) end ---@cast t table
  w:say(api.show_versions(t))
end

--
-- download given version of jdtls
--
---@param w Cmd4Lua
function M.cmd_download(w)
  w:usage('tet eclipse jdtls latest')
  local ver = w:desc("version of jdtls to download"):pop():arg()

  if not w:is_input_valid() then return end

  if ver == 'latest' then
    local ok, t = api.fetch_page_with_milestones_versions(w.vars)
    if not ok then return w:error(v2s(t)) end
    ver = ((t[#t] or {})[1] or ''):gsub('/jdtls/milestones/', '')
  end
  print('version:', ver)

  local ok, t = api.fetch_all_files_for_ver(ver, w.vars)
  if not ok or not t then return w:error(v2s(t)) end
end

--
-- install jdt-ls from tar.gz into ~/.local/share/jdtls
--
---@param w Cmd4Lua
function M.cmd_install(w)
  if not w:is_input_valid() then return end
  local dir = api.INSTALL_DIR
  local ver = 'prev' -- todo take from the current installation
  local cmd_backup = fmt('mv %s %s_%s_back', dir, ver, dir)
  local cmd_mkdir = 'mkdir -p ' .. dir
  local cmd_unzip = 'tar xf jdt-language-server-*.tar.gz -C ' .. dir
  w:say(cmd_backup)
  w:say(cmd_mkdir)
  w:say('cd path/to/dir/with/jdtls.tar.gz')
  w:say(cmd_unzip)
  w:say('task-extractor eclipse jdtls link-decompiler-engine')
  -- todo automate installation
end

--
-- create symlink to fernflower decompiler (jdtls-builtin)
--
-- https://github.com/JetBrains/intellij-community/tree/master/plugins/java-decompiler/engine
-- https://github.com/fesh0r/fernflower
--
---@param w Cmd4Lua
function M.cmd_link_decompiler_engine(w)
  local dir = w:desc("jdtls home dir"):opt("--dir", "-d")

  if not w:is_input_valid() then return end

  local ret, err = api.create_symlink_for_decompiler_engine(dir)
  if not ret then
    return w:error(err)
  end
  w:say(ret)
end

return M
