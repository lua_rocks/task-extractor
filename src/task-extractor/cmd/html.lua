-- 08-06-2024 @author Swarg
-- Goal:
-- - send HTTP GET-responses
-- - parse the html file fo find json payload by html-tag and attributes
-- - slim - testing helper to make the html files as small as possible without
--   losing the basic structure of the document
--

local log = require 'alogger'
local base = require 'cli-app-base'
local http = require "task-extractor.http"
local html = require "task-extractor.html"
local inspect = require "inspect"

--------------------------------------------------------------------------------

local M = {}

---@param w Cmd4Lua
function M.handle(w)
  w:about('interact with sql-academy.org')
      :handlers(M)

      :desc('send http request')
      :cmd('request', 'r')

      :desc('parse given html file')
      :cmd('parse', 'p')

      :desc('reduce the size as much as possible by removing child elements')
      :cmd('slim')

      :run()
end

local R = {}
--
-- send http request
--
---@param w Cmd4Lua
function M.cmd_request(w)
  w:handlers(R)

      :desc('send http GET-request to get response')
      :cmd('get', 'g')

      :desc('send http HEAD-request and show headers')
      :cmd('head', 'h')

      :run()
end

local P = {}
--
-- parse given html file
--
---@param w Cmd4Lua
function M.cmd_parse(w)
  w:handlers(P)

      :desc('find given tag in html-file')
      :cmd('get-tag-body', 'gtb')

      :desc('show the substring for given range(in bytes) in the given file')
      :cmd('get-range', 'gr')

      :run()
end

--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

--------------------------------------------------------------------------------
--                             HTTP Reguests
--------------------------------------------------------------------------------

--
-- send http GET-request to fetch given accept content
-- Experimental feature:
--  almost simultaneous request to the same address of different accepts
--  to figureout what text/html application/json returns
--
-- todo: how response headers
-- todo: define cookies and headers
-- todo: send GET-request from given filename
-- todo: send HEAD-request
--
---@param w Cmd4Lua
function R.cmd_get(w)
  local url = w:desc('url to fetch'):pop():arg()

  local accept1 = w:desc('accept type like "text/html" & "application/json"')
      :def('application/json'):opt('--accept', '-a')

  local accept2 = w:desc('to send second GET-request with given accept')
      :opt('--accept2', '-A')

  local ofn1 = w:desc('filename to save respose'):opt('--output', '-o')
  local ofn2 = w:desc('filename to save second respose'):opt('--output2', '-O')

  local log_debug = w:desc('set log level to debug')
      :has_opt('--debug', '-D')

  if not w:is_input_valid() then return end

  if log_debug then log.set_level(log.levels.DEBUG) end

  local ok1, resp1, ok2, resp2

  ok1, resp1 = http.send_get(url, accept1)
  -- experimental
  if accept2 and accept2 ~= '' then
    ok2, resp2 = http.send_get(url, accept2)
  end

  if ok1 then
    if ofn1 and type(resp1) == 'string' then
      w:say(base.write(ofn1, resp1, 'wb'), ofn1, accept1)
    else
      w:say(resp1)
    end
  else
    w:error('first accept fails for: ' .. v2s(accept1))
  end

  if ok2 then
    if ofn2 and type(resp2) == 'string' then
      w:say(base.write(ofn2, resp2, 'wb'), ofn2, accept2)
    else
      w:say("\n")
      w:say(resp2)
    end
  else
    w:error('second accept fails for: ' .. v2s(accept2))
  end
end

--
-- send http HEAD-request and show headers
--
---@param w Cmd4Lua
function R.cmd_head(w)
  w:v_opt_verbose('-v')
  local url = w:desc('url to fetch'):pop():arg()

  local accept = w:desc('accepted mime type like "text/html"')
      :opt('--accept', '-a')
  local cookie = w:desc('cookie'):opt('--cookie', '-c')

  if not w:is_input_valid() then return end

  local headers = {
    accept = accept,
    cookie = cookie,
  }
  local out = {}
  local ok, ret = http.send_head(url, headers, out)
  if not ok then
    return w:error(v2s(ret))
  end
  local data = w:is_verbose() == true and out or ret

  print(inspect(data))
end

--------------------------------------------------------------------------------


local function build_not_found(otag, attr, ctag, pstart, pend, htmlbody_len)
  return fmt('Not Found tag open: "%s" attr: %s close: %s in (%s-%s) size: %s',
    otag, v2s(attr), ctag, pstart, pend, htmlbody_len)
end

--
-- find given tag in html-file
--
---@param w Cmd4Lua
function P.cmd_get_tag_body(w)
  local fn = w:tag('fn'):desc('html filename'):pop():arg()
  local tag = w:tag('tag'):desc('tag of the html element to find'):pop():arg()
  local attr = w:tag('attr'):desc('the attribute insdide searching tag')
      :opt('--attribute', '-a')

  local ofn = w:desc('filename to save result'):opt('--output-file', '-O')
  local show = w:desc('outout the result from found range')
      :has_opt('--output', '-o')
  local show_all = w:desc('output all occurrences of the specified tag')
      :has_opt('--show-all', '-A')

  -- local single = w
  --     :desc('is tag without paired closing tag')
  --     :def(false):has_opt('--single', '-S')
  local pos_start = w
      :desc('offset position in the html-file to start the searching for a tag')
      :optn('--start', '-s')
  local pos_end = w
      :desc('the end of search - offset in byte to limit the tag search')
      :optn('--end', '-e')

  if not w:is_input_valid() then return end

  if not base.file_exists(fn) then
    return w:error('File Not Found ' .. v2s(fn))
  end
  local htmlbody = base.read_all(fn)
  if type(htmlbody) ~= 'string' then
    return w:error('Cannot Read the file ' .. v2s(fn))
  end

  local otag = '<' .. tag -- .. ' ' can ends with "\n"
  local ctag = '</' .. tag .. '>'
  local sz = #htmlbody
  pos_start = pos_start or 1
  pos_end = pos_end or #htmlbody

  -- all find all occurrences
  if show_all then
    local t = http.find_all_tags(htmlbody, otag, ctag, attr, pos_start, pos_end)
    if not t or next(t) == nil then
      return w:error(build_not_found(otag, attr, ctag, pos_start, pos_end, sz))
    end
    local opts = {
      verbose = show,
      -- todo xpath of parent elms
    }
    local res = http.get_readable_tag_ocurrences(htmlbody, t, opts)

    if ofn then
      return w:say(base.write(ofn, res, 'wb'), ofn)
    end
    return w:say(res)
  end

  -- show first
  local s, e = http.get_tag_body_range(
    htmlbody, otag, ctag, attr, pos_start, pos_end)
  if s < 0 or e < 0 then
    return w:error(build_not_found(otag, attr, ctag, pos_start, pos_end, sz))
  end

  -- save to file or print to stdout
  if ofn then
    local part = string.sub(htmlbody, s, e)
    w:say(base.write(ofn, part, 'wb'), ofn)
  end
  if show and not ofn then
    w:say(string.sub(htmlbody, s, e))
  elseif not show then
    w:say(s, e, 'len:', e - s)
  end
end

--
-- show the substring for given range(in bytes) in the given file
--
---@param w Cmd4Lua
function P.cmd_get_range(w)
  local fn = w:tag('fn'):desc('html filename'):pop():arg()
  local s = w:desc('start position (in bytes) in the file'):pop():arg()
  local e = w:desc('end position (in bytes) in the file'):optional():pop():arg()

  local ofn = w:desc('filename to save result'):opt('--output-file', '-O')

  if not w:is_input_valid() then return end

  if not base.file_exists(fn) then
    return w:error('File Not Found ' .. v2s(fn))
  end
  local content = base.read_all(fn)
  if type(content) ~= 'string' then
    return w:error('Cannot Read the file ' .. v2s(fn))
  end

  e = e or #content
  local sub = content:sub(s, e)
  if ofn then
    return w:say('saved:', base.write(ofn, sub, 'wb'), ofn)
  end
  w:say(sub)
end

--------------------------------------------------------------------------------

--
-- reduce the size as much as possible by removing child elements
--  while maintaining the overall structure of the html document
--
---@param w Cmd4Lua
function M.cmd_slim(w)
  w:v_opt_verbose('-v')
  local fn = w:desc('html file to edit'):pop():arg()
  local ofn = w:desc('attibutes in removed tag to filter'):opt('--output', '-o')
  local force_rewrite = w:desc('force to rewrite already existed')
      :has_opt('--force-rewrite', '-F')

  -- props to filter the elements in the DOM
  local tags = w:desc('list of tags to remove from html-document')
      :optl('--tags', '-t')
  local attrm = w:desc('attibutes map for filtering tags')
      :optl('--attr-map', '-m')

  if not w:is_input_valid() then return end

  local ok, ret = html.slim(fn, ofn, force_rewrite, tags, attrm, w:is_verbose())
  if not ok then
    return w:error(v2s(ret))
  end

  print(inspect(ret))
end

return M
