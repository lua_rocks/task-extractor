-- 09-06-2024 @author Swarg
-- Goal:
-- - interact with json files and data to figure out it structure
--

local inspect = require "inspect"
local base = require 'cli-app-base'
local ujson = require "task-extractor.json"
local cli = require 'task-extractor.cmd.json_cli'


local M = {}

---@param w Cmd4Lua
function M.handle(w)
  w:about('interact with sql-academy.org')
      :handlers(M)

      :desc('compare two jsons (decoded into luatable) to show the differences')
      :cmd('diff', 'd')

      :desc('open given path(of keys) in json and show it content')
      :cmd('open-path', 'op')

      :desc('interact with json like with filesystem: ls cd cat pwd')
      :cmd('interactive', 'i')

      :run()
end

--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

--
-- compare two jsons (decoded into luatable) to show the differences
--
---@param w Cmd4Lua
function M.cmd_diff(w)
  local fn1 = w:tag('fn1'):desc('filename of json or luatable'):pop():arg()
  local fn2 = w:tag('fn2'):desc('filename of json or luatable'):pop():arg()

  local path1 = w:desc('path in firts data structude'):opt('--path1', '-p')
  local path2 = w:desc('path in second data structude'):opt('--path2', '-P')
  -- local maxdeep = w:desc('max deep'):opt('--deep', '-d')
  --
  -- local ofn = w:desc('filename to save result'):opt('--output-file', '-O')

  if not w:is_input_valid() then return end

  local ok, res = ujson.compare_two_json_files(fn1, fn2, path1, path2)
  if not ok then
    return w:error(v2s(res))
  end
  w:say(fn1, '  ', fn2, "\n")
  w:say(res)
end

--
-- open given path in json and show it content
--
---@param w Cmd4Lua
function M.cmd_open_path(w)
  w:usage('tet json open-path some.json root.sub1.sub2')

  local fn = w:tag('fn'):desc('filename of json or luatable'):pop():arg()
  local path = w:desc('path in firts data structude'):pop():arg()

  if not w:is_input_valid() then return end

  local ok, t = ujson.load(fn)
  if not ok then
    return w:error(v2s(t)) -- errmsg
  end
  ---@cast t table

  local node = base.tbl_get_node(t, path)

  --
  print(inspect(node))
end

--
-- interact with json lika with filesystem: ls cd cat
--
---@param w Cmd4Lua
function M.cmd_interactive(w)
  local fn = w:tag('fn'):desc('filename of json or luatable'):pop():arg()
  local path = w:desc('"stating point" path in json to open')
      :opt('--path', '-p')
  local nk_sep = w:desc('separator to split node keys'):def('/')
      :opt('--sep', '-s')

  if not w:is_input_valid() then return end

  local ok, tbl = ujson.load(fn)
  if not ok then
    return w:error(v2s(tbl)) -- errmsg
  end
  ---@cast tbl table
  local t, t0 = tbl, nil
  path = path or ''
  if path and path ~= '' then
    ok, t0 = base.tbl_get_node(tbl, path, nk_sep)
    if not ok or type(t0) ~= 'table' then
      I.msg_not_found_node(path, nk_sep)
      return w:error(v2s(t0))
    end
    t = t0
  end

  cli.interact({ root = tbl, cwd = t, path = path, sep = nk_sep })
end

--
return M
