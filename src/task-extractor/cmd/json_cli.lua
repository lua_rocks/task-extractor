-- 10-06-2024 @author Swarg
-- Interactive mode with decoded json
-- Dependency:
-- Readline https://peterbillam.gitlab.io/pjb_lua/lua/readline.html
-- (GNU Readline/History Library)
-- TODO grep and find

local M = {}

local log = require 'alogger'
local cjson = require 'cjson'
local Cmd4Lua = require 'cmd4lua'
local base = require 'cli-app-base'
local settings = require 'task-extractor.settings'
local inspect = require "inspect"
local RL = require 'readline'


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, NULL = {}, tostring, string.format, cjson.null

-- { root = tbl, cwd = t, path = path, sep = nk_sep }
function M.handle(line, opts)
  return Cmd4Lua.of(line)
      :set_vars(opts)
      :handlers(M)

      :cmd('cd')
      :cmd('ls')
      :cmd('pwd')
      :cmd('cat')
      :cmd('set-node-sep')

      :run()
end

local commands = {
  'cd', 'ls', 'pwd', 'cat',
  'set-node-sep'
}

--------------------------------------------------------------------------------
local SHOW_STR_MAX_LEN = 60

-- configure before first use
local function setup_readline()
  local conf = settings.config or E
  local rl_conf = conf.readline or E
  RL.set_options({
    keeplines = rl_conf.keeplines or 1000,
    histfile = rl_conf.histfile, -- '~/.synopsis_history'
    completion = rl_conf.completion,
  })
  RL.set_readline_name(settings.appname)
  RL.set_complete_list(commands)
end

setup_readline()

function M.save_hits()
  RL.save_history()
end

-- cli.interact({ root = tbl, cwd = t, path = path, sep = nk_sep })
---@param opts table
function M.interact(opts)
  local state = {
    root = assert(opts.root, 'root'),
    cwd = assert(opts.cwd, 'cwd'),
    path = opts.path or '',
    sep = opts.sep or '/',
    -- prev directory
    prevd = opts.cwd,
    prevp = opts.path,
  }

  M.update_nodes_completion(state.cwd)

  while true do
    local prompt = state.path == nil and ' > ' or (state.path .. ' > ')
    local line = RL.readline(prompt)
    -- local line = base.ask_value(prompt)
    if not line or line == 'q' or line == 'exit' then break end

    M.handle(line, state)
  end

  M.save_hits()
end

--------------------------------------------------------------------------------
--                           Interactive
--------------------------------------------------------------------------------

-- interact with json in FS style: ls cd cat pwd

---@param path string
---@param cwd string
---@param sep string
function M.msg_not_found_node(path, cwd, sep, errmsg)
  return fmt("Not found '%s' in cwd:%s\n" ..
    "(Note: current node keys separator is '%s') errmsg:%s",
    v2s(path), v2s(cwd), v2s(sep), v2s(errmsg or ''))
end

---@param t table(node of decoded json)
function M.update_nodes_completion(t)
  if type(t) == 'table' then
    local c = base.tbl_deep_copy(commands)
    for k, _ in pairs(t) do
      if type(k) == 'string' then
        c[#c + 1] = k
      end
    end
    RL.set_complete_list(c)
  end
end

--
-- change (current opened) node in json
--
---@param w Cmd4Lua
function M.cmd_cd(w)
  local path = w:pop():arg()

  if not w:is_input_valid() then return end

  local ok, t = nil, nil
  local o, sep = w.vars, w.vars.sep

  -- edge cases
  if path == sep then -- root
    o.cwd = o.root
    o.path = sep
    return
    -- /sub-from-root
  elseif path:sub(1, 1) == sep then
    ok, t = base.tbl_get_node(o.root, path, sep)
    if ok then o.path = '' end
    -- up
  elseif path == '-' then
    local tmp_cwd, tmp_path = o.cwd, o.path -- to swap
    o.cwd, o.path = o.prevd, o.prevp
    o.prevd, o.prevd = tmp_cwd, tmp_path
    ---@diagnostic disable-next-line: param-type-mismatch
    M.update_nodes_completion(o.cwd)
    return
    --
  elseif string.find(path, '..', 1, true) then -- up to one or more parent nodes
    path = base.build_path(o.path, path, sep)
    if path:sub(1, 1) == sep then path = path:sub(2) end
    if path:sub(-1, -1) == sep then path = path:sub(1, -2) end
    o.cwd = o.root
    o.path = ''
    -- next step is base.tbl_get_node
  end

  local is_node_cwd = type(o.cwd) == 'table'

  -- case then key is ""(empty string)
  if path == '' and is_node_cwd and o.cwd[''] ~= nil then
    log.debug('open node with key: "" (empty string)')
    ok, t = true, o.cwd['']
  elseif is_node_cwd and tonumber(path) ~= nil and o.cwd[tonumber(path)] ~= nil
  then
    ok, t = true, o.cwd[tonumber(path)]
  elseif ok == nil then
    -- path = base.str_trim(path)
    ---@diagnostic disable-next-line: param-type-mismatch
    ok, t = base.tbl_get_node(o.cwd, path, sep)
  end

  if not ok then
    print(M.msg_not_found_node(path, o.path, sep))
  elseif type(t) ~= 'table' then
    print('Cannot change node. given path is leaf: ' .. type(t))
  else
    o.prevd, o.prevp = o.cwd, o.path
    o.cwd = t
    if o.path:sub(-1, -1) == sep then sep = '' end
    o.path = o.path .. sep .. path
    M.update_nodes_completion(t)
  end
end

--
--
--
---@param w Cmd4Lua
function M.cmd_ls(w)
  local list = w:desc('use a logn listing format')
      :defOptVal(''):opt('--list', '-l')
  local path = w:optional():pop():arg()

  if not w:is_input_valid() then return end

  local t = w.vars.cwd
  local sep = w.vars.sep

  -- case ls [-l]  path/to/subdir
  path = path or (list ~= '' and list or nil)
  if type(path) == 'string' and path ~= '' then
    local ok, t0 = base.tbl_get_node(t, path, sep)
    if not ok then
      return print(M.msg_not_found_node(path, w.vars.path, sep))
    elseif type(t0) ~= 'table' then
      return print(path, '(' .. type(t0) .. ')')
    else
      t = t0
    end
  end

  local node_keys, leaf_keys = base.tbl_keys_filter_by_type(t, 'table')
  table.sort(node_keys)
  table.sort(leaf_keys)

  local s = ''

  if list ~= nil then -- long list
    local max = base.tbl_max_str_key_len(node_keys)
    max = base.tbl_max_str_key_len(leaf_keys, max)
    local sfmt_node = '%-' .. max .. "s\n"
    local sfmt_leaf = '%-' .. max .. "s   -  %-10s  %s\n"
    for _, key in ipairs(node_keys) do
      s = s .. fmt(sfmt_node, '[' .. key .. ']')
    end
    for _, key in ipairs(leaf_keys) do
      local v, sval = t[key], ''
      local vtype = (v == NULL and 'json.null' or type(v))
      if vtype == 'boolean' or vtype == 'number' then
        sval = v2s(v)
      elseif vtype == 'string' then
        if #v > SHOW_STR_MAX_LEN then
          local maxl = SHOW_STR_MAX_LEN
          sval = '"' .. string.sub(v, 1, maxl) .. '"..(' .. (#v - maxl) .. ')'
        else
          sval = '"' .. v .. '"'
        end
        sval = sval:gsub("\n", '.')
      end
      s = s .. fmt(sfmt_leaf, key, vtype, sval)
    end
    --
  else
    -- short list
    for _, key in ipairs(node_keys) do s = s .. '[' .. v2s(key) .. '] ' end
    for _, key in ipairs(leaf_keys) do s = s .. v2s(key) .. ' ' end
  end
  print(s)
end

-- show value of the give key
---@param w Cmd4Lua
function M.cmd_cat(w)
  local path = w:pop():arg()
  local do_inspect = w:desc('inspect the node as a lua-table value')
      :has_opt('--inspect', '-i')

  if not w:is_input_valid() then return end

  local ok, t = base.tbl_get_node(w.vars.cwd, path, w.vars.sep)

  if not ok then
    print('keys:', table.concat(base.tbl_sorted_keys(w.vars.cwd), ' '))
    return print(M.msg_not_found_node(path, w.vars.path, w.vars.sep, t))
    --
  elseif type(t) == 'table' then
    if not do_inspect then
      return print(path .. ' is a node. use --inspect to show as lua table')
    end
    print(path, ':')
    print(inspect(t))
    --
  else
    print(v2s(t))
  end
end

---@param w Cmd4Lua
function M.cmd_pwd(w)
  if not w:is_input_valid() then return end
  print(w.vars.path)
end

---@param w Cmd4Lua
function M.cmd_set_node_sep(w)
  local sep = w:desc('set char to split node keys in path'):def('/'):pop():arg()
  if not w:is_input_valid() then return end
  w.vars.sep = sep
  print('undeated to', sep)
end

return M
