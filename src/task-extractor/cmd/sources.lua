-- 06-08-2024 @author Swarg
--
-- Goals:
-- - count lines-of-code

local base = require "cli-app-base"
local U = require 'task-extractor.sources'

--------------------------------------------------------------------------------

local M = {}
---@param w Cmd4Lua
function M.handle(w)
  w:handlers(M)
      :about('Interact with sources')

      :desc('calculate lines of code in given file or directory')
      :cmd('lines-of-code', 'loc')

      :desc('remove windows line endings for all files in the given directory')
      :cmd('dos2unix', 'd2u')

      :desc('remove trailing whitespaces in all files in the given directory')
      :cmd('remove-trailing-whitespace', 'rtw')

      :desc('replace all tabs in the given file or all files in the given directory')
      :cmd('replace-tab-by-spaces', 'rtbs')

      :desc("fix the permissions for all files in given directory (ntfs->ext4)")
      :cmd('fix-files-permissions', 'ffp')

      :run()
end

--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

local binary_extensions = {
  'gitignore', 'gitattributes',
  'jpg', 'jpeg', 'png', 'gif', 'bmp', 'svg', 'ico',
  'ttf', 'otf', 'eot', 'woff', 'woff2',
  'zip', 'gz', 'jar', 'class', 'wasm',
}
local ignoded_dirs = {
  '.git', 'vendor'
}

--
-- calculate lines of code in given file or directory
--
---@param w Cmd4Lua
function M.cmd_lines_of_code(w)
  w:usage('tet sources loc src/ --filter "%.c"')
  w:usage('tet sources loc . --dry-run')
  w:usage('tet sources loc . --ignored-directories [.git vendor]')
  w:usage('tet sources loc . --ignored-extensions [jpg png svg]')

  w:v_opt_dry_run('-d')
  local target = w:desc('name of file of directory to calculate'):pop():arg()
  w:desc('count empty lines'):v_has_opt('--count-empty-lines', '-e')
  w:desc('without count lines with comments'):v_has_opt('--skip-comments', '-C')
  local filter = w:desc('lua-pattern to filter filenames'):opt('--filter', '-f')

  w:desc('ignored directories'):def(ignoded_dirs)
      :v_optl('--ignored-directories', '-D')
  w:desc('ignored extensions'):def(binary_extensions)
      :v_optl('--ignored-extensions', '-E')

  local raw = w:desc('display raw data without creating a report')
      :has_opt('--raw', '-r')

  local as_json = w:desc('display raw data as json')
      :has_opt('--json', '-j')

  w:desc('make a brief report without displaying LoC for each file')
      :v_has_opt('--brief-report', '-b')

  if not w:is_input_valid() then return end

  if not base.file_exists(target) then
    return w:error('cannot find or read given path: ' .. v2s(target))
  end

  local opts, output = w.vars, nil
  opts.target, opts.cwd = target, os.getenv('PWD')
  opts.ignored_extensions = base.tbl_list_to_map(w.vars.ignored_extensions)
  opts.ignored_directories = base.tbl_list_to_map(w.vars.ignored_directories)

  if w:is_dry_run() then
    local given = { filter = filter, opts = opts }
    local skeys = base.tbl_sorted_keys
    given.opts.ignored_extensions = table.concat(skeys(opts.ignored_extensions), ' ')
    given.opts.ignored_directories = table.concat(skeys(opts.ignored_directories), ' ')
    return w:say('[DRY-RUN] ' .. (require 'inspect' (given))) -- only show given params
  end

  local files = base.list_of_files_recur(target, filter, nil, opts) or { target }
  local t = U.calculateLoCofFiles(files, opts)

  if raw then
    output = require "inspect" (t)
  elseif as_json then
    local ok, cjson = pcall(require, 'cjson')
    if not ok then return w:error(cjson) end
    output = cjson.encode(t)
  else
    output = U.mkLocReport(t, opts)
  end

  w:say(output)
end

--
-- remove windows line endings for all files in given directory
--
---@param w Cmd4Lua
function M.cmd_dos2unix(w)
  w:v_opt_dry_run('-d')
  w:v_opt_verbose('-v')
  local dir = w:desc("directory"):pop():arg()

  if not w:is_input_valid() then return end

  -- git ls-files -z | xargs -0 dos2unix

  local cmd = "find " .. dir .. " -type f -print0 | xargs -0 dos2unix -ic0 | xargs -0 dos2unix -b"
  base.exec(cmd, w:is_dry_run(), w:is_verbose())
end

--
-- remove trailing whitespaces in all files in the given directory
-- with avoid touching files that are already OK, with GNU tools:
--
---@param w Cmd4Lua
function M.cmd_remove_trailing_whitespace(w)
  local ext = w:desc("source file extension"):pop():arg();
  local only_show_files = w:desc("only show files with trailing whitespaces")
      :has_opt('--only-show', "-s")

  if not w:is_input_valid() then return end
  local cmd1, cmd2
  cmd1 = fmt("grep -rlZ --binary-files=without-match --include='*.%s' '\\s$' .", ext)
  -- local cmd = "xargs -r0 sed -i 's/\\s+$//'"
  if only_show_files then
    os.execute(cmd1 .. " | xargs -0 echo")
    return
  end

  -- this will work on GNU systems:
  cmd2 = "xargs -r0 sed -e 's/[[:blank:]]\\+$//' -i"
  local cmd = cmd1 .. " | " .. cmd2
  print('run: ' .. cmd)
  print('only for sources with file-extention:'..v2s(ext))
  os.execute(cmd)
end


--
-- replace all tabs in the given file or all files in the given directory
--
---@param w Cmd4Lua
function M.cmd_replace_tab_by_spaces(w)
  local file = w:desc("source file"):pop():arg();

  if not w:is_input_valid() then return end

  -- If using GNU grep, you can use the Perl-style regexp:
  -- grep -P '\t' *

  if not base.file_exists(file) then
    return w:error("Not found file"..v2s(file))
  end

  -- local cmd = [[sed -i.bak $'s/\t/    /g' ]] .. file
  local cmd = [[sed -i.bak 's/\t/    /g' ]] .. file
  print(cmd)
  os.execute(cmd)
end


--
-- fix permissions for all subdirs and files (ntfs->ext4)
-- made to fix broken access rights after copying files from ntfs Disk
--
---@param w Cmd4Lua
function M.cmd_fix_files_permissions(w)
  local dir = w:desc('path to directory to fix'):pop():arg()

  if not w:is_input_valid() then return end
  dir = dir or '.'

  local cmd = "find " .. dir .. " -type f -exec chmod 644 {} \\; && " ..
      "find " .. dir .. " -type d -exec chmod 755 {} \\;"

  print(cmd)
  os.execute(cmd)
end

return M
