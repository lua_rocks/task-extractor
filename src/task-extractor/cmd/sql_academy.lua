-- 06-06-2024 @author Swarg
--

local api = require 'task-extractor.api.sql_academy'
local inspect = require "inspect"

--------------------------------------------------------------------------------

local M, DB, TR, T = {}, {}, {}, {}

---@param w Cmd4Lua
function M.handle(w)
  w:about('interact with sql-academy.org')
      :handlers(M)

      :desc('Available tasks')
      :cmd('trainer', 't')

      :desc('interact with database from given task')
      :cmd('database', 'db')

      :run()
end

--
-- Available tasks
--
---@param w Cmd4Lua
function M.cmd_trainer(w)
  w:handlers(TR)

      :desc('list of trainer tasks(questions)')
      :cmd('questions-list', 'qls')

      :desc('show statistics about task difficulty, premium and companies')
      :cmd('questions-statistics', 'qs')

      :desc('get info about given question by taskId')
      :cmd('question', 'q')

      :run()
end

--
-- interact with database
--
---@param w Cmd4Lua
function M.cmd_database(w)
  w:about('interact with database from given task')
      :handlers(DB)

      :desc('inspect database infos for given task')
      :cmd('inspect', 'i')

      :desc('generate sql-ddl scheme for the database from given task')
      :cmd('scheme', 's')

      :desc('generate sql-dml insert-values for all tables of db in given task')
      :cmd('values', 'v')

      :desc('interact with table of database from given task')
      :cmd('table', 't')

  -- todo: visualize db scheme as text, open in browser (reactflow)

      :run()
end

--
-- interact with table of database from given task
--
---@param w Cmd4Lua
function DB.cmd_table(w)
  w:about('interact with table of database from given task')
      :handlers(T)

      :desc('show a raw table scheme definition')
      :cmd('inspect', 'i')

      :desc('status of the table')
      :cmd('status', 'st')

      :desc('show a names of all tables in the database from given task')
      :cmd('names', 'n')

      :desc('generate a sql-script to replicate data from a given table(dml & ddl)')
      :cmd('gen-sql', 'gs')

      :desc('generate a sql-script to create table')
      :cmd('gen-sql-ddl', 'gsd')

      :desc('visualize given table as text')
      :cmd('visualize', 'v')

      :run()
end

--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

---@param w Cmd4Lua
local function define_task_n_db_args(w)
  w:v_opt_verbose('-v')
  w:desc('work without cache'):def(false):v_has_opt('--nocache', '-NC')
  local db_id = w:desc('databaseId'):opt('--database-id', '-db')
  local task_id = w:desc('taskId'):pop():arg()
  return db_id, task_id
end

local function define_tbl_args(w)
  local db_id, task_id = define_task_n_db_args(w)
  local name = w:desc('table name'):pop():arg()

  return db_id, task_id, name
end

---@param w Cmd4Lua
---@param task_id number
---@param db_id string?
local function get_database_data(w, task_id, db_id)
  if db_id then
    error('Not implemented yet')
  end

  local ok, data = api.fetch_trainer_task_data(task_id, w.vars.nocache)
  if not ok then
    return w:error(data) -- errmsg
  end ---@cast data table
  local page_ctx = api.get_page_ctx_from_trainer_task_data(data)

  local db = api.get_database_from_trainer_task_data(data)
  if type(db) ~= 'table' then
    return w:error('Not found database in data for taskId: ' .. v2s(task_id))
  end

  api.trainer_task_cache_db_id(page_ctx, db)

  return db, page_ctx
end

---@param w Cmd4Lua
---@param name string
---@param db_id string?
local function get_table_data(w, task_id, name, db_id)
  local db, page_ctx

  db_id = db_id or api.trainer_task_get_cached_db_id(task_id)
  if not db_id then
    db, page_ctx = get_database_data(w, task_id)
    if w:has_error() then return end ---@cast db table
    db_id = db.id
  else
    db = api.trainer_task_get_cached_db(db_id)
  end
  page_ctx = page_ctx -- todo

  local ok, tbl = api.get_table_by_name(db_id, name)
  if not ok then
    return w:error(tbl) -- errmsg
  end

  return tbl, db, page_ctx
end


--
-- interact with database
-- tet sql-academy database inspect
--
---@param w Cmd4Lua
function DB.cmd_inspect(w)
  local task_id = w:desc('taskId'):pop():arg()

  if not w:is_input_valid() then return end

  local db, page_ctx = get_database_data(w, task_id)
  -- if w:has_error() then return end

  print(inspect(db), inspect(page_ctx))
end

--------------------------------------------------------------------------------

--
-- fetch database, and create scheme
--
---@param w Cmd4Lua
function DB.cmd_scheme(w)
  local task_id = w:desc('taskId'):pop():arg()
  local format = w:desc('output format'):def('sql'):opt('--format', '-f')

  if not w:is_input_valid() then return end

  local db = get_database_data(w, task_id)
  if w:has_error() then return end ---@cast db table

  if format == 'sql' then
    print(api.mk_sql_scheme_of_db(db))
  end

  if format == 'raw' then
    print(inspect(db))
  else
    print('unsupported format ' .. v2s(format))
  end
end

--
-- create sql-dml values insertions for all database tables from task
--
---@param w Cmd4Lua
function DB.cmd_values(w)
  local task_id = w:desc('taskId'):pop():arg()

  if not w:is_input_valid() then return end

  local db = get_database_data(w, task_id)
  if w:has_error() then return end ---@cast db table

  print(api.mk_sql_dml_insert_value_all_tables(db, true))
end

--------------------------------------------------------------------------------

--
-- show a raw table scheme definition
--
---@param w Cmd4Lua
function T.cmd_inspect(w)
  local db_id, task_id, tname = define_tbl_args(w)

  if not w:is_input_valid() then return end
  local _, db, _ = get_table_data(w, task_id, tname, db_id)
  if w:has_error() then return end ---@cast db table

  local t = api.get_table_data(db, tname)
  print(inspect(t))
end

--
-- status of the table
--
---@param w Cmd4Lua
function T.cmd_status(w)
  local db_id, task_id, tname = define_tbl_args(w)

  if not w:is_input_valid() then return end
  local tbl, db, _ = get_table_data(w, task_id, tname, db_id)
  if w:has_error() then return end ---@cast tbl table

  print(api.get_table_status(tname, tbl, db))
end

--
-- show a names of all tables in the database from given task
--
---@param w Cmd4Lua
function T.cmd_names(w)
  w:usage('task-extractor database table names <taskId> --verbose')
  local db_id, task_id = define_task_n_db_args(w)

  if not w:is_input_valid() then return end

  local db, _ = get_database_data(w, task_id, db_id)
  if w:has_error() then return end ---@cast db table

  local names, descs = api.get_table_names_n_desc(db)
  local s, max = '', 0

  -- format table names and discriptions(for verbose mode)
  for _, name in ipairs(names) do
    s = s .. name .. ' '
    if #name > max then max = #name end
  end

  if w:is_verbose() then
    s = s .. "\nDescriptions: \n"
    for i, desc in ipairs(descs) do
      s = s .. fmt("%-" .. max .. "s - %s\n", v2s(names[i]), v2s(desc))
    end
  end

  print(s)
end

--
-- generate sql for specified table
-- database table gen-sql
--
---@param w Cmd4Lua
function T.cmd_gen_sql(w)
  local db_id, task_id, tname = define_tbl_args(w)

  local mk_ddl = w:desc('creare ddl scheme of the table')
      :has_opt('--ddl', '-D')

  if not w:is_input_valid() then return end

  local tbl, db, _ = get_table_data(w, task_id, tname, db_id)
  if w:has_error() then return end ---@cast db table
  ---@cast tbl table

  -- generate a sql-ddl script to create the table from a given task
  if mk_ddl then
    -- local tbl = api.get_table_data(db, tname)
    local relations = api.get_tables_relations(db, tname)
    print(api.mk_sql_ddl_create_table(tname, tbl, relations))
  end

  -- generate sql-dml script to fill table(insert values) from given task

  local coltypes = api.get_coltypes_for_table(db, tname)
  print(api.mk_sql_dml_insert_values(tname, tbl, coltypes))
end

--
-- generate a sql-script to create table
--
---@param w Cmd4Lua
function T.cmd_gen_sql_ddl(w)
  local db_id, task_id, tname = define_tbl_args(w)

  if not w:is_input_valid() then return end

  local db, _ = get_database_data(w, task_id, db_id)
  if w:has_error() then return end

  -- generate a sql-ddl script to create the table from a given task
  local table_scheme = api.get_table_data(db, tname)
  local relations = api.get_tables_relations(db, tname)
  print(api.mk_sql_ddl_create_table(tname, table_scheme, relations))
end

--
-- visualize given table as text
--
---@param w Cmd4Lua
function T.cmd_visualize(w)
  local db_id, task_id, tname = define_tbl_args(w)

  if not w:is_input_valid() then return end

  local tbl, _, _ = get_table_data(w, task_id, tname, db_id)
  if w:has_error() then return end

  -- todo format as text
  print(inspect(tbl))
end

--------------------------------------------------------------------------------
--                              Trainer
--------------------------------------------------------------------------------

--
-- tasks list
--
---@param w Cmd4Lua
function TR.cmd_questions_list(w)
  w:v_opt_verbose('-v')
  w:desc('work without cache'):def(false):v_has_opt('--nocache', '-NC')
  w:desc('show inner database id'):v_has_opt('--show-dbid', '-I')
  local raw = w:desc('raw ouput'):has_opt('--raw', '-R')

  -- filters
  w:desc('degree of difficulty of the task (easy|medium|hard|any)')
      :def('easy'):v_opt('--difficulty', '-d')
  w:desc('the name of the database'):v_opt('--database', '-db')
  w:desc('show only for premium'):v_opt('--premium-only', '-P')
  w:desc('filter by given company-name'):v_opt('--company-name', '-c')
  w:desc('limit of filtered tasks'):v_opt('--limit', '-l')
  w:desc('start search from given taskId'):v_optn('--taskid-offset', '-o')

  if not w:is_input_valid() then return end

  local ok, data = api.get_trainer_questions(w.vars)
  if not ok or type(data) ~= 'table' then
    return w:error(data) -- errmsg
  end
  if raw then
    print(inspect(data))
  else
    print(api.filter_trainer_questions(data, w.vars))
  end
end

--
-- show statistics about task difficulty, premium and companies
--
---@param w Cmd4Lua
function TR.cmd_questions_statistics(w)
  w:desc('work without cache'):def(false):v_has_opt('--nocache', '-NC')

  if not w:is_input_valid() then return end

  local ok, data = api.get_trainer_questions(w.vars)
  if not ok or type(data) ~= 'table' then
    return w:error(data) -- errmsg
  end

  print(api.get_trainer_questions_statistics(data))
end

--
-- get info about given question by taskId
--
---@param w Cmd4Lua
function TR.cmd_question(w)
  w:v_opt_verbose('-v')
  w:desc('work without cache'):def(false):v_has_opt('--nocache', '-NC')
  local task_id = w:desc('taskId'):pop():argn()

  if not w:is_input_valid() then return end

  local ok, data = api.get_trainer_questions(w.vars)
  if not ok or type(data) ~= 'table' then
    return w:error(data) -- errmsg
  end

  local task = api.get_trainer_question(data, task_id)
  print(inspect(task))
end

return M
