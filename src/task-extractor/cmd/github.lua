-- 08-06-2024 @author Swarg
-- Goals:
--  - download subdirectory in given github repo without "git clone --depth 1"
--  - download single file from repo (raw)
--  - show meta info about given repository(inspect)
--  - get latest commit
--  TODO:
--  - calculate whole size of subdir before download (dry-run)
--  - check existed, already downloaded files by md5 and etag(to skip download)
--  - format toc (table of content)
--  - calculate lines of code of given subdir or whole repo
--  - show percentage of languages used

local inspect = require "inspect"
local api = require 'task-extractor.api.github'

--------------------------------------------------------------------------------

local M = {}

---@param w Cmd4Lua
function M.handle(w)
  w:about('interact with github.com')
      :handlers(M)

      :desc('show overview about given repository or subdir in the repository')
      :cmd('inspect', 'i')

      :desc('show latest commit in given repository')
      :cmd('latest-commit', 'lc')

      :desc('download sub directory in the given repo')
      :cmd('download-dir', 'dd')

      :desc('download the file in the given repo')
      :cmd('download-file', 'df')

      :desc('alive info: forks, stars, last commit')
      :cmd('alive-info', 'ai')

      :run()
end

--------------------------------------------------------------------------------

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format

--
-- show info about repository or subdir of the repository
-- TODO format toc
--
---@param w Cmd4Lua
function M.cmd_inspect(w)
  local url = w:desc('url to directory in the github-repo'):pop():arg()
  local fast_and_brief = w:desc('fast and brief via json'):has_opt('--fast', '-f')
  local no_readme = w:desc('not show the readme'):has_opt('--no-readme', '-R')
  local no_toc = w:desc('not show the TableOfContents'):has_opt('--no-toc', '-C')

  if not w:is_input_valid() then return end

  local accept = fast_and_brief == true and 'application/json' or 'text/html'

  local res, err = api.inspect_url(url, accept)
  if not res then
    return w:error(v2s(err))
  end

  if type(res) == 'table' then
    local readme = res.README
    res.README = nil
    if no_toc then res.toc = nil end
    res = inspect(res)
    if not no_readme then
      res = res .. "\n\n\nREADME\n\n" .. v2s(readme)
    end
  end

  print(v2s(res))
end

--
-- show latest commit in given repository
--
---@param w Cmd4Lua
function M.cmd_latest_commit(w)
  w:v_opt_verbose('-v')
  local url = w:desc('url to directory in the github-repo'):pop():arg()

  if not w:is_input_valid() then return end

  local ok, ret = api.get_latest_commit(url, w:is_verbose())
  if not ok then
    return w:error(v2s(ret))
  end
  if type(ret) == 'table' then ret = inspect(ret) end

  w:say(ret)
end

--
-- download subdirectory in given repo
--
---@param w Cmd4Lua
function M.cmd_download_dir(w)
  w:v_opt_verbose('-v')
  local url = w:desc('url to the directory in the github-repo'):pop():arg()
  local dst = w:desc('destination directory'):def('.'):pop():arg()

  w:desc('branch'):def('master'):v_opt('--branch', '-b')
  w:desc('mirror the repository directory structure'):v_has_opt('--mirror', '-m')
  w:desc('download all directories recursively'):v_has_opt('--recursively', '-r')

  if not w:is_input_valid() then return end

  local ok, sz, files, dirs = api.download_dir(url, dst, w.vars)
  if not ok then
    return w:error(v2s(sz))
  elseif sz == 0 then
    if files and files == 0 and dirs and dirs > 0 then
      w:say('to download all subdirectories use --recursively option')
    end
    w:fsay('downloaded files: %s dirs:%s bytes:%s', v2s(files), v2s(dirs), v2s(sz))
  end
end

--
-- download the file in the given repo
--
---@param w Cmd4Lua
function M.cmd_download_file(w)
  w:v_opt_verbose('-v')
  w:v_opt_dry_run('-d')
  local url = w:desc('url to the file in the github-repo'):pop():arg()
  local dst = w:desc('destination directory'):def('.'):pop():arg()

  w:desc('branch'):def('master'):v_opt('--branch', '-b')
  w:desc('mirror the repository directory structure'):v_has_opt('--mirror', '-m')

  if not w:is_input_valid() then return end

  local ok, sz = api.download_file(url, dst, w.vars)
  print(ok, sz)
end

--
-- alive info: forks, stars, last commit
--
---@param w Cmd4Lua
function M.cmd_alive_info(w)
  w:v_opt_verbose('-v')
  w:v_opt_dry_run('-d')
  local url = w:desc('user/repo'):pop():arg()
  w:desc('to return raw json'):tag('raw'):v_has_opt('--raw-json', '-j')
  w:desc('without last comment info'):v_has_opt('--no-last-comment', '-c')

  if not w:is_input_valid() then return end

  local t, errmsg = api.alive_info(url, w.vars)
  if not t then
    return w:error(v2s(errmsg))
  end
  print(api.fmt_alive_info(t, w.vars))
end

return M
