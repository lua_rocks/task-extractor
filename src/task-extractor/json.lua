-- 09-06-2024 @author Swarg
local log = require 'alogger'
local base = require 'cli-app-base'
local cjson = require 'cjson'

local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt, NULL = {}, tostring, string.format, cjson.null

--
--
--
---@param str_json string
function M.decode_json(str_json)
  assert(type(str_json) == 'string', 'raw_json')
  local ok, decoded = pcall(cjson.decode, str_json)
  if not ok then
    local err_msg = decoded
    log.debug('Cannot parse json responce: %s', err_msg)
    return nil
  end
  return decoded -- table
end

---@param fn string
---@param tag string?
---@return boolean
---@return string|table content or errmsg
function M.load(fn, tag)
  tag = tag or ''

  if not base.file_exists(fn) then
    return false, fmt('Not Found %s file: %s', v2s(tag), v2s(fn))
  end
  local body = base.read_all(fn)
  if type(body) ~= 'string' then
    return false, fmt('Cannot Read %s file: %s ', v2s(tag), v2s(fn))
  end

  local t = M.decode_json(body)
  if type(t) ~= 'table' then
    return false, fmt('Cannot parse %s file ', v2s(tag), v2s(fn))
  end

  return true, t
end

-- load, decode and compare two json files as luatable
---@param fn1 string
---@param fn2 string
---@param path1 string?
---@param path2 string?
function M.compare_two_json_files(fn1, fn2, path1, path2)
  local ok, t1, t2
  ok, t1 = M.load(fn1, 'first')
  if not ok then
    return false, t1 -- errmsg
  end
  ---@cast t1 table

  ok, t2 = M.load(fn2, 'second')
  if not ok then
    return false, t2 -- errmsg
  end
  ---@cast t2 table

  return M.compare_two_table(t1, t2, path1, path2)
end

--
-- compare two json decoded into luatable
--
---@param t1 table
---@param t2 table
---@param path1 string?
---@param path2 string?
function M.compare_two_table(t1, t2, path1, path2)
  local s = ''

  if path1 and path1 ~= '' then
    local ok, t = base.tbl_get_node(t1, path1)
    if not ok then return false, 'cannot open path at first: ' .. v2s(t) end
    if type(t) ~= 'table' then
      return false, 'first: expected table node got: ' .. type(t)
    end
    t1 = t
  end
  if path2 and path2 ~= '' then
    local ok, t = base.tbl_get_node(t2, path2)
    if not ok then return false, 'cannot open path at second: ' .. v2s(t) end
    if type(t) ~= 'table' then
      return false, 'second: expected table node got: ' .. type(t)
    end
    t2 = t
  end

  local keys1 = base.tbl_sorted_keys(t1, nil)
  local keys2 = base.tbl_sorted_keys(t2, nil)

  local uniqkeys1, uniqkeys2, both = {}, {}, {}
  local max = 0

  for _, key in ipairs(keys1) do
    if #key > max then max = #key end
    if t2[key] == nil then
      uniqkeys1[#uniqkeys1 + 1] = key
    else
      both[key] = 1
    end
  end
  for _, key in ipairs(keys2) do
    if #key > max then max = #key end
    if t1[key] == nil then
      uniqkeys2[#uniqkeys2 + 1] = key
    else
      both[key] = 1
    end
  end

  -- format
  local keysb = base.tbl_sorted_keys(both, nil)
  local fmsg = '%-' .. max .. 's  %-' .. max .. "s\n"

  for _, key in ipairs(keysb) do
    s = s .. fmt(fmsg, key, key)
  end
  if #uniqkeys1 > 0 or #uniqkeys2 > 0 then
    s = s .. string.rep('-', (max * 2) + 2, '') .. "\n"
    for _, key in ipairs(uniqkeys1) do
      s = s .. fmt(fmsg, key, '')
    end
    for _, key in ipairs(uniqkeys2) do
      s = s .. fmt(fmsg, '', key)
    end
  end

  return true, s
end

return M
