-- 06-06-2024 @author Swarg
local log = require 'alogger'

local base = require 'cli-app-base'
local conf = require 'task-extractor.settings'

local M = {}

function M.url_to_entry(url)
  if url and url:sub(1, 4) == 'http' then
    local host, path = string.match(url, '^https?://([^/]+)/(.+)/?$')
    if host and path then
      return host .. '/' .. path:gsub('/', '-')
    end
  end
  return url
end

function M.get_full_path(fn)
  return base.join_path((conf.CACHE_DIR or '/tmp'), fn)
end

function M.find(f, entry)
  if f then
    log.debug('find entry', entry)
    local path = M.get_full_path(M.url_to_entry(entry))
    if base.file_exists(path) then
      return base.read_all(path)
    end
  end
  return nil
end

function M.put(f, entry, data)
  if f then
    log.debug('put entry', entry)
    entry = M.url_to_entry(entry)
    local path = M.get_full_path(M.url_to_entry(entry))
    local dir = base.extract_path(path)
    if base.mkdir(dir) then
      local saved = base.write(path, data, 'wb')
      log.debug('cached to ', saved, path)
      return saved
    end
  end
  return false
end

function M.status()
  local dir = conf.CACHE_DIR
  return base.execr('du -hcd 1 ' .. dir)
end

function M.remove(entry)
  log.debug('remove entry', entry)
  entry = M.url_to_entry(entry)

  local path = M.get_full_path(M.url_to_entry(entry))
  if base.file_exists(path) then
    os.remove(path)
    return true
  end

  return false
end

-- clear all cache
function M.clear()
  log.debug('clear')

  local dir = M.get_full_path('')
  if base.dir_exists(dir) then
    error('Not implemented yet')
  end

  return false
end

return M
