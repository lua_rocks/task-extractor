-- 27-06-2024 @author Swarg
-- Goal: download jdtls Java LanguageServer
--
-- https://github.com/eclipse-jdtls/eclipse.jdt.ls
-- https://download.eclipse.org/jdtls/milestones/1.37.0/
--
-- ~/.dotfiles/nvim/.config/nvim/lua/user/lsp/settings/jdtls.lua
-- ~/.local/share/jdtls/

-- local log = require 'alogger'
local base = require 'cli-app-base'
local gumbo = require "gumbo"
local http = require "task-extractor.http"

local M = {}

local home = os.getenv('HOME') or '~'
M.INSTALL_DIR = home .. "/.local/share/jdtls"
M.DECOMPILLERS_DIR = home .. "/Decomp"


local URL_HOST = 'https://download.eclipse.org'
local REL_PATH = 'jdtls/milestones/'
local URL_JDTLS_MILESTONES = URL_HOST .. '/' .. REL_PATH


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local join_path = base.join_path


---@param url string
---@param opts table?
---@return boolean, table|string
function M.get_dirlist(url, opts)
  local accept = (opts or E).accept or 'text/html'
  local out = {}
  local ok, resp = http.send_get(url, accept, out)
  if not ok or type(resp) ~= 'string' then
    return false, out
  end

  local doc = gumbo.parse(resp)
  local dirlist = doc:getElementById("dirlist")
  -- print('[DEBUG]', dirlist.localName, dirlist.childNodes[1].localName)
  local i = M.find_next_element('table', dirlist)
  if i < 1 then
    return false, 'table not found'
  end

  local t = M.html_tbl_to_obj(dirlist.childNodes[i]) -- dirlist is a div

  table.sort(t, function(a, b) return (a[3] or '') < (b[3] or '') end)

  return true, t
end

---@param tag string
---@param node table
---@param off number?
function M.find_next_element(tag, node, off)
  off = off or 1
  if tag and tag ~= '' and type(node) == 'table' and node.childNodes then
    for i = off, #node.childNodes do
      local elm = node.childNodes[i]
      -- print('[DEBUG]', elm, 'localName:', (elm or E).localName, 'type:', elm.type)

      if elm and elm.localName == tag then
        return i
      end
    end
  end
  return -1
end

---@param opts table?
---@return boolean, table|string
function M.fetch_page_with_milestones_versions(opts)
  local ok, t = M.get_dirlist(URL_JDTLS_MILESTONES, opts)
  return ok, t
end

---@param ver string like "1.37.0"
function M.fetch_all_files_for_ver(ver, opts)
  local url = URL_JDTLS_MILESTONES .. ver
  opts.version = ver
  return M.fetch_all_files_from_dirlist(url, opts)
end

-- download all files from given "dirlist" from download.eclipse.org
---@param opts table?
---@param src_url string
function M.fetch_all_files_from_dirlist(src_url, opts)
  local ok, t = M.get_dirlist(src_url, opts)
  if not ok or type(t) ~= 'table' then return t end
  local ver = (opts or E).version or ''

  for _, row in ipairs(t) do
    local url = row[1] or ''
    if url ~= '' and url:sub(-3, -1) ~= '../' then
      if url:sub(1, 8) ~= 'https://' then
        url = URL_HOST .. url
      end
      if url:sub(-1, -1) == '/' or url:match("/[^%.]+$") then
        -- for names similar to directory names without dot like:
        -- repository, binary, features, plugins
        print('Directory: ', url)
        M.fetch_all_files_from_dirlist(url, opts)
      else
        local i = string.find(src_url, ver)
        if not i then
          return false, 'cannot extract filename for url: ' .. v2s(url)
        end
        local fn = url:sub(i + #ver + 1)
        if fn:sub(1, #REL_PATH) == REL_PATH then
          fn = fn:sub(#REL_PATH + #((opts or E).version or '') + 2)
        end
        print('[DEBUG]', url, fn)
        if string.find(fn, '/') then
          local dir = base.get_parent_dirpath(fn)
          if dir and dir ~= '' then
            base.mkdir(dir)
          end
        end

        if not base.file_exists(fn) then -- todo check sha256sum
          -- local cmd = fmt('curl "%s" -o "%s"', url, fn)
          local cmd = fmt('wget "%s" -O "%s"', url, fn)
          if not base.exec(cmd, (opts or E).dry_run, true) then
            return false, 'cannot download ' .. v2s(url)
          end
        end
      end
    end
  end

  return true, t
end

---@param t table
function M.show_versions(t)
  local s = ''
  for i = #t - 20, #t do
    -- for i = 1, #t do
    local e = t[i]
    s = s .. fmt("%3s  %s  %s  %s\n", i, v2s(e[1]), v2s(e[2]), v2s(e[3]))
  end
  return s
end

--[[
<tr>
  <td></td>
  <td>
      <img src='//dev.eclipse.org/small_icons/places/folder.png' />
      <a href='/jdtls/milestones/0.1.0'> 0.1.0</a>
  </td>
  <td style="text-align: right; padding-right:15px;"> 30.2M </td>
  <td>2017-11-02 15:43</td>
  <td></td>
</tr>
]]
-- parse node with HtmlTable into lua-table
---@param tbl_elm table
function M.html_tbl_to_obj(tbl_elm)
  local t = {}
  --                   table       tbody         <tr>-s
  for _, tr in ipairs(tbl_elm.childNodes[1].childNodes) do
    if tr.localName == 'tr' then -- tag
      local row = {}
      if type(tr.childNodes) == 'table' then
        for _, td in ipairs(tr.childNodes) do
          if td.childNodes then
            local suff = ''
            for _, elm in ipairs(td.childNodes) do
              if elm.localName == 'a' then
                row[#row + 1] = (elm:getAttribute('href')) .. suff
                --
              elseif elm.localName == 'img' then
                local src = elm:getAttribute('src')
                if src and string.find(src, 'folder.png') then suff = '/' end
                --
              elseif elm.localName == nil then
                local s = (elm.textContent) -- innerHTML
                if s ~= '' and s ~= " " then
                  row[#row + 1] = s
                end
                -- print('type:', elm.type) whitespace|text
              end
            end
          end
        end
      end
      t[#t + 1] = row
    end
  end
  return t
end

local INTEREST_PLUGINS = {
  'org.eclipse.jdt.ls.core_', -- show version of the jdtls itself
  'org.junit_',
  -- build systems:
  'org.gradle.toolingapi_', -- used gradle
  'org.eclipse.m2e.maven.',
  'org.apache.ant',
  --
  'jakarta' -- JavaEE
}

function M.get_installed_version(all)
  -- ./plugins/org.eclipse.jdt.ls.core_1.37.0.202406271335.jar
  if not base.dir_exists(M.INSTALL_DIR) then
    return 'not found install dir ' .. v2s(M.INSTALL_DIR)
  end
  local f = '^.*%.jar$'
  local dir = base.join_path(M.INSTALL_DIR, 'plugins')
  local jars, err = base.list_of_files(dir, f, { basenames = true })
  if not jars then
    return v2s(err)
  end
  local s = ''
  if all then
    for _, jar in ipairs(jars) do
      s = s .. jar .. "\n"
    end
  else
    -- longer but maintaining the order specified in the interest_plugins
    for _, name in ipairs(INTEREST_PLUGINS) do
      for _, jar in ipairs(jars) do
        if jar:sub(1, #name) == name then
          s = s .. jar .. "\n"
        end
      end
    end
  end
  return s
end

-------------------------------------------------------------------------------

-- ~/.local/share/jdtls/config_linux/org.eclipse.osgi/57/0/.cp/lib/java-decompiler-engine-231.9011.34.jar
-- org.jetbrains.java.decompiler
--
---@return false|string?
---@return string? errmsg
function M.create_symlink_for_decompiler_engine(homedir)
  local dir = homedir or (join_path(M.INSTALL_DIR, "config_linux", "org.eclipse.osgi"))
  if not base.dir_exists(dir) then
    return false, 'directory not exists: ' .. v2s(dir)
  end
  local jarname = 'java-decompiler-engine-*.jar'
  local path = (base.execrl(fmt("find %s -type f -name '%s'", dir, jarname)) or E)[1]
  if not path then
    return false, 'not found ' .. jarname
  end

  local decomp_dir = join_path(M.DECOMPILLERS_DIR, 'jb-fernflower')
  if not base.mkdir(decomp_dir) then
    return false, 'cannot create ' .. v2s(decomp_dir)
  end
  local linkname = join_path(decomp_dir, 'decompiler-engine.jar')
  os.execute('unlink ' .. linkname)

  local ret = base.execr(fmt("ln -s %s %s 2>&1", path, linkname))
  if not ret or #ret < 4 or string.find(ret, 'fail') ~= nil then
    return false, v2s(ret)
  end

  return linkname, nil
end

return M
