-- 06-06-2024 @author Swarg
-- Utils to parse response from https://sql-academy.org/ru/trainer/tasks/1

local log = require 'alogger'
local cjson = require 'cjson'
local http = require "task-extractor.http"
local cache = require 'task-extractor.cache'
local zlib = require 'zlib'


local M = {}

M.HOST = 'sql-academy.org'
M.TRAINER = 'https://sql-academy.org/en/trainer'
M.TRAINER_TASK = 'https://sql-academy.org/en/trainer/tasks/'
M.TRAINER_TASK_RU = 'https://sql-academy.org/ru/trainer/tasks/'
M.SANDBOX_ENDPOINT = "https://sql-academy.org/api/v1/sandbox"
M.QUESTIONS = "https://" .. M.HOST .. "/api/v1/questions?sort=byIncreasingDifficulty"


local query_tmpl = '{"sql":"%s","database":"%s"}'
local SCRIPT_DATA_TAG = '<script id="__NEXT_DATA__" type="application/json"'

local TYPES_TO_WRAP = {
  VARCHAR = true,
  CHAR = true,
  DATE = true,
  DATETIME = true,  -- for MySQL
  TIMESTAMP = true, -- PostreSQL used TIMESTAMP instead of DATETIME
}


---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format


function M.fetch_trainer_task_data(task_id, nocache)
  log.debug("fetch_task_data taskid:", task_id)

  assert(type(task_id) ~= nil, 'tasknr')

  local url = M.TRAINER_TASK .. tostring(task_id)
  local st, resp
  resp = cache.find(not nocache, url)

  if not resp then
    st, resp = http.send_get(url, 'text/html')
    if not st then
      return false, 'Cannot fetch ' .. url
    end
    cache.put(not nocache, url, resp)
  end

  return M.parse_trainer_task_response(resp)
end

--
--
--
---@param resp string?
function M.parse_trainer_task_response(resp)
  if type(resp) ~= 'string' then
    return false, 'expected string response got ' .. v2s(resp)
  end

  local pstart, pend = http.get_tag_body_range(resp, SCRIPT_DATA_TAG)
  if pstart < 0 or pend < 0 then
    return false, 'Cannot find the script block'
  end

  local script = string.sub(resp, pstart, pend)

  local obj = http.decode_json(script)

  return (type(obj) == 'table'), obj --(inspect(obj))
end

---@return table?
---@param t table?
function M.get_database_from_trainer_task_data(t)
  local irs = ((((t or E).props or E).pageProps or E).initialReduxState) or E
  local entry = ((((irs.trainer or E).questions or E).questions or E)[1] or E)
  return entry.database
end

function M.get_page_ctx_from_trainer_task_data(t)
  local page = t.page   -- "/trainer/tasks/[taskId]"
  local query = t.query -- { taskId = "14" },

  if page and query then
    page = string.gsub(page, '%[taskId%]', query.taskId, 1)
  end
  return {
    page = page,
    taskId = query.taskId
  }
end

---@param s string
---@param page_ctx table?
function M.mk_cache_key(s, page_ctx)
  assert(type(s) == 'string' and s ~= '', 's')
  local path = ((page_ctx or E).page or ''):gsub('/', '-')
  return M.HOST .. '/' .. path .. s
end

---@param page_ctx table
---@param db table
function M.trainer_task_cache_db_id(page_ctx, db)
  assert(type(page_ctx) == 'table', 'page_ctx')
  assert(type(db) == 'table', 'db')
  assert(db.id ~= nil and db.id ~= '', 'db.id')

  local entry_uri_to_dbid = M.mk_cache_key('-db_id', page_ctx)
  local entry_dbid_to_uri = M.mk_cache_key(db.id, nil)
  local ok1 = cache.put(true, entry_uri_to_dbid, db.id)
  local ok2 = cache.put(true, entry_dbid_to_uri, entry_uri_to_dbid)
  log.debug('cached db.id', ok1, ok2)
  return ok1 and ok2
end

function M.trainer_task_get_cached_db_id(taskId)
  local page_ctx = {
    '/trainer/tasks/' .. v2s(taskId)
  }
  local entry = M.mk_cache_key('-db_id', page_ctx)
  return cache.find(true, entry)
end

---@param db_id string
function M.trainer_task_get_cached_db(db_id)
  assert(type(db_id) == 'string', 'db_id')
  local path = cache.find(true, M.mk_cache_key(db_id))
  if path then
    print('##TODO:', path)
  end
end

--------------------------------------------------------------------------------

--
---@param database_id string
---@param name string
function M.get_table_by_name(database_id, name)
  assert(type(database_id) == 'string', 'database_id')
  assert(type(name) == 'string', 'name')

  local key = M.mk_cache_key(database_id .. '_' .. name)
  local data = cache.find(true, key)

  if not data then
    local url = M.SANDBOX_ENDPOINT
    local sql = string.format("SELECT * FROM " .. name .. '')
    local query = string.format(query_tmpl, sql, database_id)

    -- "SELECT * FROM Trip"
    local post_body = query

    local st, resp_body = http.send_post(url, post_body, 'application/json')
    if not st or type(resp_body) ~= 'string' then
      return false, 'error on send post: ' .. url
    end

    cache.put(true, key, resp_body)
    data = resp_body
  end

  local t = http.decode_json(data) -- table
  if not t then
    return false, 'cannot decode json for database_id:'
        .. v2s(database_id) .. ' table:' .. v2s(name)
  end
  if not t.fields or not t.rows then
    return false, 'wrong response expected object with fields and rows'
  end

  return true, t
end

-- table raw scheme for mk_sql_ddl_create_table
---@param db table?
---@param tblname string
function M.get_table_data(db, tblname)
  assert(type(tblname) == 'string', 'tblname')
  tblname = string.lower(tblname)

  for _, t in ipairs((db or E).tables) do
    if type(t) == 'table' and type(t.id) == 'string' then
      if tblname == string.lower(t.id) then
        return t
      end
    end
  end
  return nil
end

---@param db table
---@return table names
---@return table descriptions
function M.get_table_names_n_desc(db)
  local t, desc = {}, {}
  for _, tbl in ipairs((db or E).tables) do
    if type(tbl) == 'table' and type(tbl.id) == 'string' then
      t[#t + 1] = tbl.id
      desc[#desc + 1] = tbl.description or ''
    end
  end
  return t, desc
end

function M.get_coltypes_for_table(db, tblname)
  local t = M.get_table_data(db, tblname)
  if t then
    local ct = {}
    for _, p in ipairs(t.props) do
      if p.name then
        ct[p.name] = string.upper(p.type or '')
      end
    end
    return ct
  end
  return nil
end

local OPPOSIT_COORDINALITY = {
  ['one-to-many'] = 'many-to-one',
  ['many-to-one'] = 'one-to-many',
  ['one-to-one'] = 'one-to-one',
}

function M.get_tables_relations(db, tblname)
  assert(type(tblname) == 'string', 'tblname')
  tblname = string.lower(tblname)

  -- relations = { {
  --   id = UUID,
  --   type = "one-to-many",
  --   source = { field = "id", table = "company" },
  --   target = { field = "company", table = "trip" }
  -- },
  local rels = {}

  for _, t in ipairs((db or E).relations) do
    -- ==<E  >>  <==
    if type(t) == 'table' and type(t.target) == 'table' then
      if tblname == string.lower(t.target.table) then
        rels[t.target.field] = {
          to_table = t.source.table,
          to_column = t.source.field,
          type = OPPOSIT_COORDINALITY[t.type or false] or 'one-to-many'
        }
        -- for one-to-one make UNIQUE KEY in this table
      end
    end
  end

  return rels
end

---@param table_scheme table?
---@param colname string
function M.get_column_pros(table_scheme, colname)
  if table_scheme then
    for _, t in ipairs((table_scheme or E).props or E) do
      if type(t) == 'table' and t.name == colname then
        return t
      end
    end
  end
  return nil
end

---@param name string
---@param tbl table
---@diagnostic disable-next-line: unused-local
function M.get_table_status(name, tbl, db)
  assert(type(tbl) == 'table', 'tbl')

  local fields = tbl.fields
  local rows = tbl.rows

  local s = 'table: "' .. v2s(name) .. '" columns: '

  for i, field in ipairs(fields) do
    if i > 1 then s = s .. ' ' end
    s = s .. field
  end

  s = s .. "\nRows: " .. #(rows or E)

  -- calculate crc32 of all table values
  do
    local compute1 = zlib.crc32()
    local len = 0
    for _, row in ipairs(rows) do
      local line = ''
      -- join all values as string and calculate crc32
      for _, v in pairs(row) do
        if v ~= cjson.null then
          line = line .. v2s(v)
        end
      end
      compute1(zlib.crc32()(line))
      len = len + #line
    end

    local code, code2 = compute1()
    s = s .. ' CRC32: ' .. v2s(code) .. ' ' .. code2 .. ' length: ' .. len
  end

  return s
end

--- insert into <table> (columns) values (values)
---@param name string
---@param obj table decoded json from api
---@param coltypes table?
---@return string
function M.mk_sql_dml_insert_values(name, obj, coltypes)
  assert(type(obj) == 'table' and obj.fields and obj.rows, 't')

  local fields = obj.fields
  local rows = obj.rows

  local s = 'INSERT INTO ' .. name .. ' ('

  for i, field in ipairs(fields) do
    if i > 1 then s = s .. ', ' end
    s = s .. field
  end

  s = s .. ")\nVALUES\n"

  local rows_cnt = #rows
  for r, row in ipairs(rows) do
    local line = '('
    for i, k in ipairs(fields) do
      if i > 1 then line = line .. ', ' end
      local value = row[k] or 'NULL'
      if coltypes then
        -- wrap strings
        local coltyp = coltypes[k]
        if TYPES_TO_WRAP[coltyp] then
          value = "'" .. value .. "'"
        end
      end
      line = line .. value -- .. ', '
    end
    if r < rows_cnt then
      line = line .. "),\n"
    else
      line = line .. ")\n"
    end
    s = s .. line
  end

  return s .. ";"
end

---@param table_scheme table? - decoded json with table infos
---@param relations table?
---@return string
function M.mk_sql_ddl_create_table(name, table_scheme, relations)
  if type(table_scheme) ~= 'table' then return 'no-table' end

  local sz = #table_scheme.props
  local coldefs, max = {}, 0

  for i, p in ipairs(table_scheme.props) do
    local field = p.name
    -- local p = M.get_column_pros(table_scheme, field) or E
    local coltype = p.type or 'VARCHAR'
    local l = field .. ' ' .. coltype
    if p.isKey then
      l = l .. ' PRIMARY KEY'
    end
    if i < sz then
      l = l .. ','
    end
    if #l > max then max = #l end
    coldefs[i] = l
    -- s = s .. "\n"
  end

  local columns = ''
  -- descriptions
  for i, p in ipairs(table_scheme.props) do
    local line = coldefs[i]
    local desc = ''
    if type(p.description) == 'string' then
      local pad = max - #line
      if pad > 0 then
        desc = string.rep(' ', pad, '')
      end
      desc = desc .. "  -- " .. p.description
    end
    columns = columns .. "  " .. line .. desc .. "\n"
  end

  local s = 'CREATE TABLE IF NOT EXISTS ' .. name .. " (\n" .. columns

  if relations and next(relations) then
    s = s .. "\n"
    for k, v in pairs(relations) do
      if type(v) == 'table' and v.to_table and v.to_column then
        s = s .. fmt("  FOREIGN KEY (%s) REFERENCES %s(%s),\n",
          k, v.to_table, v.to_column)
      end
    end
    s = s:sub(1, -3) .. "\n"
    -- CONSTRAINT student_group_fk
    --   FOREIGN KEY (group_id) REFERENCES Groups(group_id),
  end

  return s .. ");"
end

--
-- create full scheme of the database
---@param db table
function M.mk_sql_scheme_of_db(db)
  local s = ''
  if type(db.title) == 'string' then
    s = s .. '-- ' .. db.title .. "\n"
  end
  if type(db.description) == 'string' then
    s = s .. '-- ' .. db.description .. "\n"
  end
  if type(db.name) == 'string' then
    s = s .. '-- CREATE DATABASE IF NOT EXISTS ' .. db.name .. ";\n"
    s = s .. "-- USE " .. db.name .. ";\n"
    s = s .. "-- \\connect " .. db.name .. " -- for PostreSQL\n"
  end

  for _, t in ipairs(db.tables) do
    if type(t) == 'table' then
      local relations = M.get_tables_relations(db, t.title or t.id)
      s = s .. M.mk_sql_ddl_create_table(t.title or t.id, t, relations) .. "\n\n"
    end
  end

  return s
end

function M.mk_sql_dml_insert_value_all_tables(db, verbose)
  local s = ''
  local db_id = assert(db.id, 'db.id')
  local sz = #db.tables

  for i, pt in ipairs(db.tables) do
    if type(pt) == 'table' then
      local table_name = pt.title or pt.id
      if verbose then
        print(fmt('[%s/%s] fetching table: %s ...', i, sz, v2s(table_name)))
      end

      local ok, t = M.get_table_by_name(db_id, table_name)
      if ok and type(t) == 'table' then
        local coltypes = M.get_coltypes_for_table(db, table_name)
        s = s .. M.mk_sql_dml_insert_values(table_name, t, coltypes) .. "\n\n"
      end
    end
  end
  return s
end

--------------------------------------------------------------------------------
--                              Trainer
--------------------------------------------------------------------------------
local function verbose(opts, ...)
  if (opts or E).verbose then
    local arg_cnt = select('#', ...)
    local s = ''
    for i = 1, arg_cnt do
      if i > 1 then s = s .. ' ' end
      s = s .. tostring(select(i, ...))
    end

    print(s)
  end
end

function M.get_trainer_questions(opts)
  opts = opts or {}
  local st, json
  local url = M.QUESTIONS
  verbose(opts, 'url:', url)
  json = cache.find(not opts.nocache, url)

  if not json then
    st, json = http.send_get(url, 'application/json')
    if not st or type(json) ~= 'string' then
      return false, 'Cannot fetch url: ' .. v2s(url)
    end
    cache.put(not opts.nocache, url, json)
  end
  local obj = http.decode_json(json)

  return obj ~= nil, obj
end

---@param qt table
---@param opts table{difficulty}
function M.filter_trainer_questions(qt, opts)
  assert(type(qt) == 'table', 'expected parsed json')
  local difficulty = opts.difficulty or 'any'
  local any_difficulty = difficulty == 'any' or difficulty == '*'
  local premium_only = opts.premium_only == nil and false or opts.premium_only
  local company_name = opts.company_name or '*'
  local limit = type(opts.limit) == 'number' and opts.limit or false

  local s, c, stop_by_limit = '', 0, false
  local st = { easy = 0, medium = 0, hard = 0 }

  for _, t in ipairs(qt) do
    if type(t) == 'table' then
      -- statiscics
      if t.difficulty then
        st[t.difficulty] = (st[t.difficulty] or 0) + 1
      end

      -- filter
      if (any_difficulty or t.difficulty == difficulty) and
          (premium_only and t.premiumOnly or not premium_only) and
          (company_name == '*' or t.companyName == company_name) then
        s = s .. M.format_question(t, opts) .. "\n"
        c = c + 1
        if limit and c > limit then
          stop_by_limit = true
          break
        end
      end
    end
  end

  -- how types
  s = s .. "Tasks by difficulty: "
  for k, v in pairs(st) do
    s = s .. v2s(k) .. ': ' .. v2s(v) .. " "
  end
  s = s .. " total count: " .. v2s(#qt)

  if stop_by_limit then s = s .. "\nstop_by_limit: " .. v2s(limit) end

  return s
end

local function fmt_company_stats(name, ct)
  local d = ''
  for k, v in pairs(ct.difficulty) do
    d = d .. v2s(k) .. ': ' .. v2s(v) .. " "
  end

  local l = fmt("%20s: %8s %8s  %-30s ",
    v2s(name), (ct.opened or '0'), (ct.premium or '0'), d)

  -- databases
  for k, v in pairs(ct.dbs) do
    l = l .. v2s(k) .. ': ' .. v2s(v) .. " "
  end

  return l .. "\n"
end

--
--
function M.get_trainer_questions_statistics(qt)
  assert(type(qt) == 'table', 'expected parsed json')

  local s = ''
  local companies = {}
  local st = { easy = 0, medium = 0, hard = 0 }

  for _, t in ipairs(qt) do
    if type(t) == 'table' then
      -- statiscics
      if t.difficulty then
        st[t.difficulty] = (st[t.difficulty] or 0) + 1
      end
      local companyName = t.companyName or 'noname'
      companies[companyName] = companies[companyName] or {}
      local ct = companies[companyName]
      if t.premiumOnly then
        ct.premium = (ct.premium or 0) + 1
      else
        ct.opened = (ct.opened or 0) + 1
      end
      ct.difficulty = ct.difficulty or {}
      ct.dbs = ct.dbs or {}
      local degree = t.difficulty or '?'
      ct.difficulty[degree] = (ct.difficulty[degree] or 0) + 1

      local dbname = (t.database or E).title or 'no-title'
      -- print("[DEBUG] ct:", require"inspect"(t))
      ct.dbs[dbname] = (ct.dbs[dbname] or 0) + 1
    end
  end

  -- how types
  s = s .. "Companies:\n"
  s = s .. fmt("%20s: %8s %8s  %-30s Databases\n",
    'CompanyName', 'opened', 'premium', 'Difficulty')

  local keys = http.sorted_keys(companies, function(a, b) return a > b end)

  s = s .. fmt_company_stats('noname', companies.noname)

  -- for name, ct in pairs(companies) do
  for _, name in ipairs(keys) do
    local ct = companies[name]
    if name ~= 'noname' then
      s = s .. fmt_company_stats(name, ct)
    end
  end

  s = s .. "\nTasks by difficulty: "
  for k, v in pairs(st) do
    s = s .. v2s(k) .. ': ' .. v2s(v) .. " "
  end
  s = s .. " total: " .. v2s(#qt)

  return s
end

---@param t table
---@param opts table?
---@diagnostic disable-next-line: unused-local
function M.format_question(t, opts)
  local company = t.companyName or ''
  local db = (t.database or E).title or '?'
  local p = t.premiumOnly == true and '[premium]' or ''
  local dbid = ''
  if (opts or E).show_dbid then
    dbid = t._id ~= nil and '(' .. v2s(t._id) .. ')' or '?'
  end
  -- todo html format
  local question = t.question

  return fmt("#%s [%s]%s db:'%s'%s %s:\n%s\n",
    v2s(t.id), v2s(t.difficulty), p, v2s(db), dbid, v2s(company), v2s(question))
end

---@param qt table
---@param task_id number
function M.get_trainer_question(qt, task_id)
  assert(type(task_id) == 'number', 'task_id')
  for _, t in ipairs(qt) do
    if t.id == task_id then
      return t
    end
  end
  return nil
end

---@param resp string
---@return boolean, string|table?
function M.parse_trainer_main_page(resp)
  local pstart, pend = http.get_tag_body_range(resp, SCRIPT_DATA_TAG)

  if pstart < 0 or pend < 0 then
    return false, 'Cannot find the script block'
  end

  local script = string.sub(resp, pstart, pend)

  local obj = http.decode_json(script)

  return obj ~= nil, obj --(inspect(obj))
end

return M
