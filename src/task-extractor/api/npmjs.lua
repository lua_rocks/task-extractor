-- 03-09-2024 @author Swarg
-- nodejs.com api
--
-- https://github.com/npm/registry/blob/main/docs/download-counts.md
-- https://stackoverflow.com/questions/34071621/query-npmjs-registry-via-api

local log = require 'alogger'
local base = require 'cli-app-base'
local HttpClient = require 'lhttp.Client'
local github_api = require 'task-extractor.api.github'


local M = {}
--
M.HOST_PACKAGE = "https://www.npmjs.com/package/"
M.REGISTRY_HOST = "https://registry.npmjs.com/"
M.API_HOST = "https://api.npmjs.org/"

local E, v2s, fmt = {}, tostring, string.format
local log_debug = log.debug
local key_count = base.tbl_key_count
local sorted_keys = base.tbl_sorted_keys
local first_word = base.str_first_word

--
---@param t table
---@param opts table?
---@return string
function M.fmt_alive_info(t, opts)
  opts = opts or {}
  opts.max_key_len = opts.max_key_len or 16
  opts.ignore_keys = opts.ignore_keys or {}
  opts.ignore_keys.repository_info = 1

  local s = base.tbl_format_as_list(t, M.alive_info_keys, opts)
  if type(t.repository_info) == 'table' and not opts.raw then
    s = s .. "\n" .. github_api.fmt_alive_info(t.repository_info, opts)
  end
  return s
end

M.alive_info_keys = {
  'error',
  'name',
  'version',
  'homepage',
  'repository',
  'keywords',
  'description',
  'author',
  'maintainers',
  'license',
  'dependencies',
  'peerDependencies',
  'devDependencies',
  'nodeVersion',
  'npmVersion',
  -- dist
  'unpackedSize',
  'fileCount',
  'tarball',
  'shasum',

  -- alive stat
  'last_publish',
  'downloads',
}

---@param n number
local function n2rCnt(n, opts)
  if type(n) == 'number' and not (opts or E).full_numbers then
    return base.numcnt_to_readable(n)
  end
  return n
end
local function n2rSize(n, opts)
  if type(n) == 'number' and not (opts or E).full_numbers then
    return base.numsize_to_readable(n)
  end
  return n
end

--
---@param pkgname string
---@param opts table?{full_numbers, period}
---@return false|table
---@return string? errmsg
function M.alive_info(pkgname, opts)
  if type(pkgname) ~= 'string' or pkgname == '' then
    return false, 'no package name'
  end

  opts = opts or E

  local client = HttpClient:new()
  local period = opts.period or 'last-week'
  local url = fmt('%sdownloads/point/%s/%s', M.API_HOST, v2s(period), pkgname)
  local downloads = client:getJson(url)
  if type(downloads) ~= 'table' then
    return false, 'not found: "' .. v2s(pkgname) .. '"'
  end
  if downloads.error then
    return false, downloads.error -- package x not found
  end
  local url2 = M.REGISTRY_HOST .. pkgname .. '/latest'
  local t = client:getJson(url2)

  if type(t) ~= 'table' then
    return false, 'fetch error from ' .. url2
  end

  local o
  if opts.raw then
    o = {
      registry = t,
      downloads = downloads,
    }
  else
    local readable_downloads = ''
    if downloads then
      readable_downloads = fmt('%s (%s %s)', n2rCnt(downloads.downloads, opts),
        v2s((downloads).start), v2s((downloads)['end']))
    end
    o = {
      name = t.name or pkgname,
      version = t.version,
      keywords = M.readable_keywords(t.keywords),
      description = t.description,
      author = (t.author or E).name or t.author,
      license = t.license,
      homepage = t.homepage,
      repository = (t.repository or E).url,
      -- dist
      fileCount = (t.dist or E).fileCount,
      unpackedSize = n2rSize((t.dist or E).unpackedSize),
      tarball = (t.dist or E).tarball, -- tgz
      shasum = (t.dist or E).shasum,   -- sha1sum of file with tarball archive

      npmVersion = t._npmVersion,
      nodeVersion = t._nodeVersion,
      last_publish = M.getLastPublishFromOpInternal(t),

      maintainers = M.readable_mainterners(t.maintainers),
      dependencies = key_count(t.dependencies or E),
      devDependencies = key_count(t.devDependencies or E),

      downloads = readable_downloads
    }
    if t.peerDependencies then
      o.peerDependencies = table.concat(sorted_keys(t.peerDependencies), ' ')
    end
  end

  if opts.repo_info and (t.repository or E).url then
    o.repository_info = github_api.alive_info(t.repository.url, opts)
  end
  return o
end

---@param t table
---@return string
function M.readable_keywords(t)
  local s = ''
  if type(t) == 'table' then
    for _, word in ipairs(t) do
      if s ~= '' then s = s .. ' ' end
      s = s .. string.gsub(word, '%s', '-') --string.find(word, ' ', 1, true)
    end
  end
  return s
end

---@param t table{{name:string, email:string}}
---@return string
function M.readable_mainterners(t)
  if type(t) == 'table' then
    local s = #t .. ':'
    for _, e in ipairs(t) do
      if e.name then
        s = s .. ' ' .. v2s(e.name)
      end
    end
    return s
  end
  return '?'
end

--
-- "tmp": "tmp/abc_1.2.3_1724429433123_0.34211381570954136" ->  2024-08-23
--
---@return string? date
---@return string? package
---@return string? version
function M.getLastPublishFromOpInternal(t)
  if type(t) == 'table' then
    local tmp = (t._npmOperationalInternal or E).tmp or ''
    local p, v, ts = string.match(tmp, 'tmp/([^_]+)_([^_]+)_(%d+)_')
    if ts then
      ts = (tonumber(ts) or 0) / 1000
      return tostring(os.date("%Y-%m-%d", ts)), p, v
    end
  end
  return nil
end

--------------------------------------------------------------------------------

-- todo
--
---@param names table
---@param opts table?{notify}
function M.comparison_table(names, opts)
  log_debug("comparison_table", names)
  local tbl = {}
  opts = opts or {}
  opts.repo_info = true -- add repository_info
  local notify = type(opts.notify) == 'function' and opts.notify or nil

  local cap = {
    ['package'] = 3, -- pkg
    downloads = 5,
    repository = 8,
    stars = 1,
    forks = 1,
    created_at = 7,
    last_commit = 7,
  }
  tbl[1] = cap
  local check_maxlen = base.str_check_maxlen

  for i, pkgname in ipairs(names) do
    if notify then
      notify(fmt("%s/%s: %s", i, #names, pkgname)) -- to show progress
    end

    local t, err = M.alive_info(pkgname, opts)
    local e = {
      ['package'] = pkgname
    }

    if type(t) == 'table' then
      local repo_info = (t.repository_info or E)
      e.downloads = first_word(t.downloads)
      e.repository = v2s(repo_info.full_name or t.repository or '?')
      e.stars = v2s(repo_info.stars)
      e.forks = v2s(repo_info.forks)
      e.last_commit = string.sub(repo_info.last_commit or t.last_publish or '', 1, 7)
      e.created_at = string.sub(repo_info.created_at or '', 1, 7)
      -- extra_infos for footnote below table
      e.description = repo_info.description
      e.topics = repo_info.topics
      e.homepage = t.homepage
      e.author = t.author
      e.maintainers = t.maintainers
      e.repourl = t.repository

      -- max for aligment
      check_maxlen(cap, 'package', pkgname)
      check_maxlen(cap, 'downloads', e.downloads)
      check_maxlen(cap, 'repository', e.repository)
      check_maxlen(cap, 'stars', e.stars)
      check_maxlen(cap, 'forks', e.forks)
    else
      e.err = err
    end
    tbl[#tbl + 1] = e
  end

  return tbl
end

-- local comparison_table_cap_keys = { }
function M.fmt_comparision_table(t, opts)
  opts = opts or {}
  opts.format = opts.format or 'markdown'
  if opts.format == 'markdown' or opts.format == 'md' then
    return M.fmt_comparision_table_markdown(t, opts)
  else
    return 'unsupported format: ' .. v2s(opts.format) .. ' raw:' .. inspect(t)
  end
end

-- format data to readable mark
---@param t table{}
---@param opts table?{minify, no_footnotes}
function M.fmt_comparision_table_markdown(t, opts)
  opts = opts or E
  if type(t) == 'table' then
    local c = t[1]
    local rowp = opts.minify == true and
        "|%%-%ds|%%%ds|%%-%ds|%%%ds|%%%ds|%%%ds|%%%ds|\n" or
        "| %%-%ds | %%%ds | %%-%ds | %%%ds | %%%ds | %%%ds | %%%ds |\n"

    local sfmt = fmt(rowp,
      c['package'], c.downloads, c.repository, c.stars, c.forks, c.last_commit, c.created_at)
    local align = base.str_align_center
    local cpkg = align('package', c, 'pkg')
    local cdwlds = align('downloads', c, 'dwlds')
    local crepo = align('repository', c, 'repo')
    local cstars = align('stars', c, '*')
    local cforks = align('forks', c, 'f')

    -- header with columns names
    local s = fmt(sfmt, cpkg, cdwlds, crepo, cstars, cforks, "lcommit", "created")

    -- head-body sep line
    s = s .. fmt(sfmt,
      string.rep('-', c['package'] or 1),
      string.rep('-', c.downloads or 1),
      string.rep('-', c.repository or 1),
      string.rep('-', c.stars or 1),
      string.rep('-', c.forks or 1),
      string.rep('-', c.last_commit or 1),
      string.rep('-', c.created_at or 1))

    local footnotes = ""

    for i = 2, #t, 1 do
      local e = t[i]
      s = s .. fmt(sfmt,
        e['package'],
        e.downloads,
        e.repository,
        e.stars,
        e.forks,
        e.last_commit,
        e.created_at
      )
      if not opts.no_footnotes then
        footnotes = footnotes ..
            fmt("\n- %s %s\n(%s) %s\nauthor: %s  %s\ndescrp: %s\ntopics: %s\n",
              v2s(e['package']), v2s(e.homepage),
              v2s(e.created_at), v2s(e.repourl or '?'),
              v2s(e.author or '?'), v2s(e.maintainers or 'no maintainers'),
              v2s(e.description or 'no description'),
              v2s(e.topics or 'no topics')
            )
      end
    end
    return s .. footnotes
  end
  return v2s(t)
end

return M
