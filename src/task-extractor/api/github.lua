-- 08-06-2024 @author Swarg
-- Goals:
--  - download given subdirectory from github repo without cloning all repo
--  - overview given repository by url or user/repo

local log = require 'alogger'
local base = require 'cli-app-base'
local HttpClient = require 'lhttp.Client'
local http = require "task-extractor.http"
local cjson = require 'cjson'
-- local gumbo = require "gumbo" -- optional

local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
local starts_with = base.str_starts_with
local log_debug = log.debug

local NULL = cjson.null
local APP_JSON = http.APP_JSON

local HOST_GITHUB = "https://github.com/"
local HOST_RAW_CONTENT = "https://raw.githubusercontent.com/"
-- to parse main page of the repo
local TAG_REACTPARTIAL = "<react-partial"
local TAG_REACTPARTIAL_CLOSE = "</react-partial>"
local ATTR_REPOS_OVERVIEW = 'partial-name="repos-overview"'

-- to parse code-view in the subdirs of the repo
local TAG_REACTAPP = '<react-app'
local TAG_REACTAPP_CLOSE = '</react-app>'
local ATTR_REACT_CODE_VIEW = 'app-name="react-code-view"'
-- <react-app app-name="react-code-view" initial-path="/<GITHUB_USER>/<REPO>/tree/master/<SUBDIR>"

-- <script type="application/json" data-target="react-partial.embeddedData"
-- <script type="application/json" data-target="react-app.embeddedData"

--
-- org/repo                             -->  https://github.com/org/repo
-- https://github.com/org/repo          -->  https://github.com/org/repo
-- git+https://github.com/org/repo.git  -->  https://github.com/org/repo
--
---@param fullname string?
---@return boolean
---@return string url|errmsg
function M.mk_full_repo_url(fullname)
  local c = base.str_substr_count(fullname, "/")
  if c < 1 then ---@cast fullname string
    return false, 'expected at least user/repo'
  end
  if c == 1 then -- mk full url
    fullname = HOST_GITHUB .. fullname
  elseif starts_with(fullname, "git+http") then
    fullname = string.match(fullname, "^git%+(.-)%.git$")
  elseif starts_with(fullname, "git://") then
    local uri = string.match(fullname, "^git://(.-)%.git$")
    assert(type(uri) == 'string' and uri ~= '', 'uri for ' .. v2s(fullname))
    fullname = 'https://' .. uri
  end
  return true, fullname
end

local function n2rCnt(n)
  if type(n) == 'number' and n > 1000 then
    return base.numcnt_to_readable(n)
  end
  return n
end


---@param html string
---@return boolean ok
---@return string|table errmsg or decoded json
function M.get_code_view_data(html)
  assert(type(html) == 'string', 'html')

  local ps0, pe0 = http.get_tag_body_range(html,
    TAG_REACTAPP, TAG_REACTAPP_CLOSE, ATTR_REACT_CODE_VIEW)

  if ps0 < 0 or pe0 < 0 then
    return false, 'Not Found react-code-view tag'
  end

  local ps, pe = http.get_tag_body_range(html, '<script ', nil, nil, ps0, pe0)
  if ps < 0 or pe < 0 then
    local details = html:sub(ps0, ps0 + 160) .. "\n...\n" .. html:sub(pe0 - 80, pe0)

    return false, fmt('Not Found react-code-view embeddedData script (%s-%s)',
      v2s(ps0), v2s(pe0)) .. "\n" .. details
  end

  local t = http.decode_json(html:sub(ps, pe))

  return type(t) == 'table', t or 'decode error'
end

--
--
--
---@param url string
---@param opts table?
---@return boolean ok
---@return string|table errmsg or decoded json
function M.fetch_page_code_view_data(url, opts)
  local accept = (opts or E).accept or 'text/html'
  local out = {}
  local ok, resp = http.send_get(url, accept, out)
  if not ok or not resp then
    if (opts or E).verbose then
      print(inspect(out))
    end
    return false, 'Cannot fetch ' .. v2s(url) .. ' err:' .. resp
  end

  if (opts or E).accept == http.APP_JSON then
    local obj = http.decode_json(resp)
    if type(obj) ~= 'table' then
      if (opts or E).verbose then
        print(inspect(out)) -- code, status, headers
        print(inspect(resp))
      end
      return false, 'Cannot decode json from ' .. v2s(url)
    end
    return true, obj
  else
    return M.get_code_view_data(resp) -- boolean, table(decoded-json) or errmsg
  end
end

--------------------------------------------------------------------------------

--
-- find payload(json) from html
--
---@param html string?
---@return boolean, string|table?
function M.get_repos_overview_data(html)
  log.debug("get_repos_overview_data")
  assert(type(html) == 'string', 'html')

  local ps0, pe0 = http.get_tag_body_range(html,
    TAG_REACTPARTIAL, TAG_REACTPARTIAL_CLOSE, ATTR_REPOS_OVERVIEW)

  if ps0 < 0 or pe0 < 0 then
    return false, 'Not Found react-partial tag'
  end

  local ps, pe = http.get_tag_body_range(html, '<script ', nil, nil, ps0, pe0)
  if ps < 0 or pe < 0 then
    local details = html:sub(ps0, ps0 + 160) ..
        "\n...\n" .. html:sub(pe0 - 80, pe0)

    return false, fmt('Not Found react-partial embeddedData script (%s-%s)',
      v2s(ps0), v2s(pe0)) .. "\n" .. details
  end

  local t = http.decode_json(html:sub(ps, pe))

  return type(t) == 'table', t or 'decode error'
end

--------------------------------------------------------------------------------

--
-- in:  'https://github.com/user/repo/tree/master/path/in/project' ->
-- out: 'user', 'repo', 'master', 'path/in/project'
--
---@param url string
---@return string? user
---@return string? repo
---@return string? branch
---@return string? path
function M.parse_github_url(url)
  local user, repo, branch, path
  -- local ptrn = '^https://github.com/([^%s/]+)/([^%s/]+)([^%s/]*)(.*)'
  local ptrn = '^https://github.com/([^%s/]+)/([^%s/]+)(.*)'
  user, repo, path = string.match(url, ptrn)
  if path and path ~= '' then
    -- /tree/master/subdir/file --> "master" "subdir/file"
    branch, path = string.match(path, '^/[^%s/]+/([^%s/]+)/(.*)$')
  else
    branch, path = nil, nil
  end
  return user, repo, branch, path
end

-- https://github.com/lunarmodules/luasql/blob/master/doc/us/doc.css
-- https://raw.githubusercontent.com/lunarmodules/luasql/master/doc/us/doc.css
---@param user string
---@param repo string
---@param path string
---@param branch string?
function M.get_url_to_raw_file(user, repo, path, branch)
  assert(type(user) == 'string', 'user')
  assert(type(repo) == 'string', 'repo')
  assert(type(path) == 'string', 'path')
  branch = branch or 'master'
  return fmt('%s%s/%s/%s/%s', HOST_RAW_CONTENT, user, repo, branch, path)
end

---@param opts table
local function download_item(url, dst, opts)
  local print0 = (opts or E).print or print
  print0(dst, '<--', url)
  if base.file_exists(dst) then
    print0('[WARN] rewrite already existed file')
    -- TODO send HEAD to check is file does not changed
  end
  local dir = base.get_parent_dirpath(dst)
  if dir and not base.dir_exists(dir) then
    base.mkdir(dir)
  end
  local ok, sz = http.download_file(url, dst)
  return ok, sz
end

---@param items table
---@param dst_dir string
---@param opts table{user_n_repo, branch, mirror, recursively, print0}
local function download_items(items, dst_dir, opts)
  local files, dirs, totalsz = 0, 0, 0
  local user, repo, branch = opts.user, opts.repo, opts.branch

  for _, item in ipairs(items) do
    if type(item) == 'table' then
      local path = assert(item.path, 'item.path')
      local name = assert(item.name, 'item.name')

      if item.contentType == 'file' then
        files = files + 1
        local url0 = M.get_url_to_raw_file(user, repo, path, branch)
        if opts.mirror then name = path end

        local dst0 = base.join_path(dst_dir, name)

        local ok2, sz = download_item(url0, dst0, opts)
        if not ok2 then
          return false, 'stop at ' .. path .. ' ' .. v2s(sz) -- errmsg
        end
        if type(sz) == 'number' then totalsz = totalsz + sz end
        --
      elseif item.contentType == 'directory' then
        dirs = dirs + 1
        if opts.recursively then
          local dir = (opts.mirror == true) and path or name
          local url0 = fmt('%s%s/%s/tree/%s/%s', HOST_GITHUB, user, repo, branch, path)
          local dst_subdir = base.join_path(dst_dir, dir)

          if dst_subdir and not base.dir_exists(dst_subdir) then
            base.mkdir(dst_subdir)
          end
          log_debug('download subdir:', url0)
          local ok2, sz, files0, dirs0 = M.download_dir(url0, dst_subdir, opts)

          if ok2 and type(sz) == 'number' then
            totalsz, files, dirs = totalsz + sz, files + files0, dirs + dirs0
          else
            return false, sz -- 'errmsg'
          end
        end
      end
    end
  end

  return true, totalsz, files, dirs
end
--
--
---@param opts table?{branch, recursively, mirror}
function M.download_dir(url, dst_dir, opts)
  opts = opts or {}

  local user, repo, _ = M.parse_github_url(url)
  if not user and repo then
    return false, 'expected url to github user/repo got: ' .. v2s(url)
  end

  if not base.dir_exists(dst_dir) then
    return false, 'destination directory not exists ' .. v2s(dst_dir)
  end

  local ok, data = M.fetch_page_code_view_data(url, {
    accept = APP_JSON, verbose = opts.verbose
  })

  if not ok then
    return false, data
  end
  local items = (((data or E).payload or E).tree or E).items
  if type(items) ~= 'table' then
    return false, 'cannot find items for url ' .. url
  end

  return download_items(items, dst_dir, {
    branch = opts.branch or 'master',
    mirror = opts.mirror == true,
    recursively = opts.recursively,
    print = opts.print or print,
    user = user,
    repo = repo,
  })
end

--
---@param url string
---@param dst string
---@param opts table
function M.download_file(url, dst, opts)
  local user, repo, branch, path = M.parse_github_url(url)
  if not user or not repo or not path or path == '' then
    return false, 'expected url to github user/repo got: ' .. v2s(url)
  end

  local url0 = M.get_url_to_raw_file(user, repo, path, branch)
  local fn = (opts.mirror == true) and path or base.basename(path)

  if base.dir_exists(dst) then
    dst = base.join_path(dst, fn)
  end

  local ok, sz = download_item(url0, dst, opts)
  return ok, sz
end

--------------------------------------------------------------------------------


-- url to main repo page or to subdir in the repo
function M.get_latest_commit(url, verbose)
  local ok, resp = http.send_get(url, http.APP_JSON)
  if not ok or not resp then
    return false, 'Cannot fetch ' .. v2s(url)
  end
  local t = http.decode_json(resp)
  if type(t) ~= 'table' then
    return false, 'cannot parse json'
  end
  local payload = (t.payload or E) or E
  local refInfo = payload.refInfo

  if not refInfo then
    return false, 'not found payload.refInfo'
  end
  if verbose then
    return true, refInfo
  end
  return refInfo.currentOid ~= nil, refInfo.currentOid
end

-- todo use app/json accept

-- url to main repo page or to subdir in the repo
---@param url string
function M.inspect_url(url, accept)
  log.debug("inspect_url", url, accept)
  local ok, resp
  ok, url = M.mk_full_repo_url(url)
  if not ok then
    return false, url -- err
  end
  -- fetch data
  accept = accept or 'text/html' -- 'application/json'
  ok, resp = http.send_get(url, accept)
  if not ok or not resp then
    return false, 'Cannot fetch ' .. v2s(url)
  end

  base.save_dump_on('TET_SAVE_RESP', resp, 'response to %s %s', url, accept)
  log.debug('Response size: ', #(resp or ''))

  local t
  if accept == http.APP_JSON then
    t = http.decode_json(resp)
    if type(t) ~= 'table' then
      return false, 'cannot parse json'
    end
    local _, _, branch, _ = M.parse_github_url(url)
    base.save_dump_on('TET_SAVE_RESP', resp)
    if not branch then
      return M.inspect_repos_overview_data(t)
    end
  end

  ok, t = M.get_repos_overview_data(resp)
  if ok then
    return M.inspect_repos_overview_data(t)
  end
  local errmsg = v2s(t)

  ok, t = M.get_code_view_data(resp)
  if ok and type(t) == 'table' then
    return M.inspect_code_view_data(t)
  end
  errmsg = errmsg .. "\n" .. v2s(t)

  return 'unknown type of the url: ' .. url .. "\n" .. errmsg
end

---@param data any -- table?
function M.inspect_repos_overview_data(data)
  if type(data) ~= 'table' then
    return 'no repos-overview data'
  end
  local d
  d = data.payload
  local from_api = type(d) == 'table'
  if not d then
    if type(data.props) ~= 'table' then
      return 'no repos-overview props'
    end
    d = data.props.initialPayload
    if not d then return 'no payload' end
  end

  local t = {}
  t.repo = d.repo
  t.refInfo = d.refInfo -- branch commit hash
  -- currentOid is current commit hash
  t.commitCount = (d.overview or E).commitCount
  t.overviewFiles = {}
  t.files = {}



  if from_api then -- fetched via application/json not via text/html
    t.title = data.title
    local readme = (d.tree or E).readme
    if type(readme) == 'table' then
      t.README = readme.richText
      t.toc = (readme.headerInfo or E).toc -- table of contents
    end
  else
    -- readme and licence
    for _, tt in ipairs((d.overview or E).overviewFiles or E) do
      if tt.path and tt.tabName then
        if tt.tabName == 'README' then
          if tt.richText and tt.richText ~= NULL then
            t.README = tt.richText
          end
        else
          local s = tt.tabName
          if tt.path ~= s and tt.path then
            s = s .. ' (' .. tt.path .. ')'
          end
          t.overviewFiles[#t.overviewFiles + 1] = s .. ''
        end
      end
    end
  end

  -- files in root of repo
  for _, item in ipairs(((d.tree or E).items or E)) do
    if type(item) == 'table' and item.path then
      local q, p = '', ''
      if (item.contentType == 'directory') then q, p = '[', ']' end
      t.files[#t.files + 1] = q .. v2s(item.path) .. p
    end
  end

  -- html to plain text
  if t.README then
    local ok, gumbo = pcall(require, "gumbo")
    if ok then
      local doc = gumbo.parse(t.README)
      t.README = doc.body.textContent or t.README
    end
  end

  return t
end

--
-- convert raw json to human readable
--
---@param data table?
function M.inspect_code_view_data(data)
  if type(data) ~= 'table' then
    return 'no code-view data'
  end
  local t = {}
  local payload = data.payload
  t.repo = (payload or E).repo

  return t
end

function M.fmt_alive_info(t, opts)
  return base.tbl_format_as_list(t, M.alive_info_keys, opts)
end

M.alive_info_keys = {
  'full_name',
  'archived',
  'description',
  'topics',
  'last_commit',
  'created_at',
  'updated_at',
  'pushed_at',
  'forks',
  'stars',
  'subscribers',
  'open_issues',
  'language',
  'license',
}

---@param t any (table)
---@return any
local function get_tbl_key(t, key)
  if type(t) == 'table' then
    return t[key]
  end
  return nil
end

---@return string?
local function get_tbl_key_concat(t, key, sep)
  local v = get_tbl_key(t, key)
  if type(v) == 'table' then
    return table.concat(v, sep or ' ')
  end
  return nil
end

---@param fullname string user/repo  or https://github.com/user/repo
---@param opts table?
---@return false|table
---@return string?
function M.alive_info(fullname, opts)
  assert(type(fullname) == 'string' and fullname ~= '', 'url')
  opts = opts or E
  local ok
  ok, fullname = M.mk_full_repo_url(fullname)
  if not ok then
    return false, v2s(fullname) -- err
  end

  local client = HttpClient:new()
  local url = string.gsub(fullname, "/github.com", '/api.github.com/repos')

  -- accept: application/vnd.github.preview
  local t = client:acceptJson():get(url):getDecodedBody()
  if type(t) ~= 'table' then
    return false, 'cannot get encoded json body'
  end

  -- https://github.com/orgs/community/discussions/24442
  -- pushed_at  - represents the date and time of the last commit,
  -- (will be updated any time a commit is pushed to any of the repository's branches)
  --
  -- updated_at - represents the date and time of the last change the the repository.
  -- updated_at - not only for commit, but it may also be other things, such as
  -- changing the description of the repo, creating wiki pages, etc.
  local o = opts.raw == true and t or {
    archived = t.archived,
    full_name = t.full_name,
    description = t.description,
    topics = get_tbl_key_concat(t, 'topics', ' '),
    created_at = t.created_at,
    updated_at = t.updated_at,
    pushed_at = t.pushed_at,
    forks = n2rCnt(t.forks),
    stars = n2rCnt(t.stargazers_count),
    language = t.language,
    license = get_tbl_key(t, 'license'),
    open_issues = t.open_issues,
    subscribers = t.subscribers_count,
  }

  if not opts.no_last_comment then
    local commits_url = url .. '/commits?per_page=1'
    local tc = client:acceptJson():get(commits_url):getDecodedBody()

    if type(tc) == 'table' then
      local commit = (tc[1] or E).commit or E
      local date = (commit.author or E).date or (commit.committer or E).date

      if opts.raw then
        o.last_commit = {
          message = commit.message,
          data = date,
          sha = (tc[1] or E).sha or (commit.tree or E).sha
        }
      else
        o.last_commit = date
      end
    else
      log.debug('cannot fetch commit info from %s', commits_url)
    end
  end

  return o
end

return M
