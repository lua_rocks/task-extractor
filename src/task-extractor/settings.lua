-- 06-06-2024 @author Swarg
--
-- App Configuration
--
local log = require 'alogger'

local base = require 'cli-app-base'
local builtin = require 'cli-app-base.builtin-cmds'

local M = {}
local join = base.join_path

M.appname = 'task-extractor'
M.config_file = nil
M.config = nil

M.CONF_DIR = os.getenv('HOME') .. '/.config/' .. M.appname .. '/'
M.SHARE_DIR = os.getenv('HOME') .. '/.local/share/' .. M.appname .. '/'
M.CACHE_DIR = os.getenv('HOME') .. '/.cache/' .. M.appname .. '/'

local default_config = {
  editor = 'nvim',
  logger = {
    save = true,
    level = log.levels.INFO,
    appname = M.appname
  },
  readline = {
    histfile = join(M.CONF_DIR, 'rl_hist'),
    keeplines = 1000,
    completion = true,
  },
}

---@param conf table
local function apply_env_vars(conf)
  conf = conf
  local debug_log_level = os.getenv('TASK_EXTRACTOR_DEBUG')
  if debug_log_level then
    log.fast_setup()
  end
end

function M.setup()
  M.config, M.config_file = base.setup(
    default_config, M.CONF_DIR, M.SHARE_DIR, apply_env_vars
  )
  -- bind self to buildint commands such as logger, config
  builtin.settings = M
end

--
-- save config file
--
---@param verbose boolean?
---@return boolean
function M.save(verbose)
  local f = base.save_config(M.config_file, M.config)
  local msg = M.config_file .. ' saved: ' .. tostring(f)
  if verbose then
    print(msg)
  end
  return f
end

return M
