-- 06-06-2024 @author Swarg
-- Utils to work with http requests
--

local log = require 'alogger'
-- local base = require 'cli-app-base'

local zlib = require 'zlib' -- for gzip  luarocks lua-zlib
local cjson = require 'cjson'
local http = require 'socket.http'
local ltn12 = require 'ltn12'
local inspect = require 'inspect'

local M = {}

---@diagnostic disable-next-line: unused-local
local E, v2s, fmt = {}, tostring, string.format
M.APP_JSON = 'application/json'

--
-- download GET responce for given url
--
---@param url string
---@param headers_or_accept table|string|nil
function M.send_get(url, headers_or_accept, out)
  log.debug("send_get", url, headers_or_accept)
  assert(type(url) == 'string', 'url')
  -- expected_content = expected_content or 'text/xml; charset=UTF-8'
  local headers, expected_content

  if type(headers_or_accept) == 'string' then
    headers = {
      accept = headers_or_accept, -- expected_content or M.APP_JSON,
      ['accept-encoding'] = "gzip",
    }
    expected_content = headers.accept
  elseif type(headers_or_accept) == 'table' then
    headers = headers_or_accept
  end

  local response_body = {}
  local req = {
    scheme = "https",
    method = 'GET',
    url = url,
    headers = headers,
    sink = ltn12.sink.table(response_body),
  }

  local r, code, resp_headers, status = http.request(req)
  if type(out) == 'table' then
    out.req = req
    out.resp = { code = code, status = status, headers = resp_headers }
  end

  if not r or code ~= 200 then
    return false, 'response code:' .. v2s(code) .. ' ' .. v2s(status)
  end

  -- parse json response
  local content_type = (resp_headers or E)['content-type']
  if expected_content then
    if not content_type or not string.find(content_type, expected_content) then
      return false, 'unexpected content-type: ' .. tostring(content_type)
    end
  end

  local respbody = table.concat(response_body)

  if (resp_headers or E)["content-encoding"] == "gzip" then
    log.debug('ungzip content')
    local inflated, eof, bytes_in, bytes_out = zlib.inflate()(respbody)
    respbody = eof == true and inflated or '' -- ?
    log.debug('ungziped:', eof, bytes_in, bytes_out)
  end

  log.debug("resp body len:", #respbody)

  return true, respbody
end

--
-- send POST request
--
---@param url string
---@param post_body string
function M.send_post(url, post_body, accept)
  log.debug("send_post")
  assert(type(url) == 'string', 'url')

  local response_body = {}

  local source = ltn12.source.string(post_body);

  local new_reqt = {
    scheme = "https",
    method = 'POST',
    url = url,
    headers = {
      accept = accept or 'application/json',
      --
      ['content-type'] = 'application/json',
      ['content-length'] = tostring(#post_body),
      ['accept-encoding'] = "gzip",
    },
    sink = ltn12.sink.table(response_body),
    source = source
  }
  log.debug('POST', new_reqt.url)
  local r, code, headers, status = http.request(new_reqt)
  if not r then
    log.debug('http.req err:' .. tostring(code))
    return false
  end

  if code ~= 200 then
    print('[DEBUG]', inspect(new_reqt))
    print('[DEBUG]', post_body)
    print('[DEBUG] code:', code, status)
    log.debug('response code is', code, status)
    return false
  end

  -- parse json response
  local content_type = (headers or E)['content-type']
  if not content_type or accept and not string.find(content_type, accept) then
    log.debug('Not Supported Content-Type:%s expected:%s ', content_type, accept)
    return false
  end

  local resp_body = table.concat(response_body)

  if (headers or E)["content-encoding"] == "gzip" then
    log.debug('ungzip content')
    local inflated, eof, bytes_in, bytes_out = zlib.inflate()(resp_body)
    resp_body = eof == true and inflated or '' -- ?
    log.debug('ungziped:', eof, bytes_in, bytes_out)
  end

  log.debug("resp-body len:", #resp_body)

  return true, resp_body
end

function M.gzip_decompress_file(fn)
  log.debug('ungzip file', fn)

  local source, errmsg = io.open(fn)
  if not source then
    return false, 'cannot read file ' .. v2s(fn) .. ' ' .. v2s(errmsg)
  end
  local fn_tmp = fn .. '.ungzip'
  local dest, err = io.open(fn_tmp, 'wb') --io.tmpfile()
  if not dest then
    source:close()
    return false, 'cannot create new tmp file ' .. v2s(err)
  end

  local inflate = zlib.inflate()
  local shift = 0
  local total_bytes = 0

  while true do
    local data = source:read(4096)
    if not data then break end
    local inflated, eos, bytes_in, bytes_out = inflate(data)
    if type(bytes_out) == 'number' then
      total_bytes = total_bytes + bytes_out
    end
    dest:write(inflated)
    if eos then
      source:seek("set", shift + bytes_in)
      shift = shift + bytes_in
      inflate = zlib.inflate()
    end
  end

  source:close()
  dest:close()
  os.remove(fn)
  os.rename(fn_tmp, fn)

  return true, total_bytes
end

---@param url string
---@param dst string
---@param headers table?
function M.download_file(url, dst, headers, out)
  log.debug("download_file", url, dst, headers)
  assert(type(url) == 'string', 'url')

  -- local response_body = {}
  local req = {
    scheme = "https",
    method = 'GET',
    url = url,
    headers = headers or {
      -- accept = 'application/json',
      ['accept-encoding'] = "gzip",
    },
    sink = ltn12.sink.file(io.open(dst, 'wb'))
  }
  local r, code, resp_headers, status = http.request(req)
  if type(out) == 'table' then
    out.req = req
    out.resp = { code = code, status = status, headers = resp_headers }
  end

  local h = resp_headers or E

  if not r or code ~= 200 then -- todo 204 partial
    local details = ''
    if code == 301 then details = ' location:' .. v2s(h['location']) end
    return false, 'code: ' .. tostring(code) .. ' ' .. status .. details
  end

  local total_bytes = 0

  if h["content-encoding"] == "gzip" then
    local ok, sz = M.gzip_decompress_file(dst)
    if not ok then
      return false, sz -- errmsg
    end
    if ok and type(sz) == 'number' then
      total_bytes = sz
    end
  else
    local cont_len = tonumber(h['content-length'])
    if cont_len then
      total_bytes = cont_len
    end
  end

  return true, total_bytes
end

--
-- about given url based on HEAD request
--
---@param url string
---@param headers table?
---@param out table?
function M.send_head(url, headers, out)
  log.debug("send_head")
  assert(type(url) == 'string', 'url')
  local resp = {}
  local req = {
    scheme = "https",
    method = 'HEAD',
    url = url,
    headers = headers or {},
    sink = ltn12.sink.table(resp)
  }
  local r, code, resp_headers, status = http.request(req)
  if type(out) == 'table' then
    out.req = req
    out.resp = { code = code, status = status, headers = resp_headers }
  end

  local h = resp_headers or E

  if not r or code ~= 200 then
    local details = ''
    if code == 301 then details = ' location:' .. v2s(h['location']) end
    return false, 'code: ' .. tostring(code) .. ' ' .. status .. details
  end

  return true, {
    size = tonumber(h['content-length']),
    ctype = h['content-type'],
    etag = h.etag,
    cache_control = h['cache-control']
  }
end

--
--
--
---@param str_json string
function M.decode_json(str_json)
  assert(type(str_json) == 'string', 'raw_json')
  local ok, decoded = pcall(cjson.decode, str_json)
  if not ok then
    local err_msg = decoded
    log.debug('Cannot parse json responce: %s', err_msg)
    return nil
  end
  return decoded -- table
end

--
-- a simplified way to get the body range(substring) of a given html-tag
-- If an 'attr' is specified and 'tag_open' is not closed by a ">" symbol, then
-- find and return the range of only those tags whose attributes include the
-- specified substring 'attr'
--
-- by default, if the tag and attribute are not specified, searches for the
-- body of the first script-element in the given html-document
--
---@param html string
---@param tag_open string?   -- <script ...  or <script>
---@param tag_close string?  -- </script>
---@param attr string?       -- optional used if tag_open NOT ends with '>'
---@param off number?        -- start position in the html to find
---@param range_end number?  -- end position  to limit the search area)
---@return number, number
function M.get_tag_body_range(html, tag_open, tag_close, attr, off, range_end)
  log.debug("get_tag_body_range html:%s open:%s close:%s offset:%s",
    html ~= nil, tag_open, tag_close, off)
  if html then
    -- range
    off = off or 1                 -- start
    range_end = range_end or #html -- end

    -- tag to find
    tag_open = tag_open or '<script'
    tag_close = tag_close or '</script>'

    local closed_opend_tag = tag_open:sub(-1, -1) == '>'
    local found = false
    local pstart
    local has_attr = type(attr) == 'string' and attr ~= '' and attr ~= ' '

    -- by design, in order to also search by attribute inside the opening tag,
    -- the opening tag must not end with the '>' symbol
    if closed_opend_tag and has_attr then
      log.debug('Cannot find closed tag_open:"%s" for attr:"%s" ', tag_open, attr)
      return -1, -1
    end

    while true do
      pstart = string.find(html, tag_open, off, true)
      if not pstart or pstart > range_end then
        log.debug('Not Found content with tags "%s","%s"', tag_open, tag_close)
        return -1, -1
      end
      pstart = pstart + #tag_open

      if not closed_opend_tag then -- open tag is not closed - can check attr
        local i = string.find(html, '>', pstart, true)
        if not i or i > range_end then
          log.debug('Cannot find end of the tag:"%s" end:%s', tag_open, range_end)
          return -1, -1
        end
        found = true

        if attr and attr ~= '' then
          local attr_offset = string.find(html:sub(pstart, i), attr, 1, true)
          if not attr_offset then
            found = false
            off = i + 1
          else
            pstart = i + 1
            break
          end
        end
        pstart = i + 1
        --
      elseif attr and attr ~= '' then
        -- case then cannot check attr between <tag ... & >
        log.debug('Got an open_tag with ">" and attr', tag_open)
        found = false
        return -1, -1
      end

      if not attr or closed_opend_tag then
        break
      end
      -- try to find next given pair tag + attr
    end

    if not found then
      log.debug('Not Found tag:"%s" with attr:"%s"', tag_open, attr)
      return -1, -1
    end

    local pend = string.find(html, tag_close, pstart, true)
    if not pend or pend > range_end then
      log.debug('Not Found the tag_close:"%s" from pos: %s', tag_close, pstart)
      return -1, -1
    end

    return pstart, (pend - 1)
  end
  return -1, -1
end

---@param htmlbody string
---@param otag string
---@param ctag string
---@param attr string?
---@param pstart number?
---@param pend number?
---@return table
function M.find_all_tags(htmlbody, otag, ctag, attr, pstart, pend)
  assert(type(htmlbody) == 'string', 'htmlbody')
  local res = {}
  local off = pstart or 1
  pend = pend or #htmlbody

  while true do
    local s, e = M.get_tag_body_range(htmlbody, otag, ctag, attr, off)
    if s < 0 or e < 0 then
      break
    end
    res[#res + 1] = { s, e } -- start and end position
    off = e + 1
  end

  return res
end

--
-- create a readable report of found occurrences of tags
--
---@param html string
---@param positions table ranges
---@param opts table?
function M.get_readable_tag_ocurrences(html, positions, opts)
  assert(type(html) == 'string', 's')
  assert(type(positions) == 'table', 't')
  local verbose = (opts or E).verbose
  -- todo show parent element, or xpath to root

  local s = ''
  for _, t in ipairs(positions) do
    local ps, pe = t[1], t[2]
    s = s .. v2s(ps) .. '-' .. v2s(pe)
    if verbose then
      s = s .. ' ' .. string.sub(html, ps, pe)
    end
    s = s .. "\n"
  end
  if verbose then
    s = s .. 'Total: ' .. #positions
  end

  return s
end

return M
