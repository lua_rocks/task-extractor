-- 06-08-2024 @author Swarg
-- Goals:
--  - count LinesOfCode in given directory

local base = require "cli-app-base"

local M = {}


local E, v2s, fmt = {}, tostring, string.format
local get_ext = base.extract_extension



---@param files table
---@param opts table
function M.calculateLoCofFiles(files, opts)
  local t = {}

  t.__ignored = {}
  local ignored_extensions = (opts or E).ignored_extensions or {}

  for _, fname in ipairs(files) do
    local ext = get_ext(fname)
    if not ignored_extensions[ext or 0] then
      t[#t + 1] = M.calculateLoC(fname, opts)
    else
      -- count amount of another files
      t.__ignored[ext or '?'] = (t.__ignored[ext or '?'] or 0) + 1
    end
  end
  return t
end

function M.calculateLoC(fname, opts)
  local f, err = io.open(fname)
  local count_empty = opts.count_empty_lines == true
  -- local skip_comments = opts.skip_comments == true -- todo
  if f then
    local c = 0
    while true do
      local line = f:read("*l")
      if not line then
        break
      end
      if line ~= '' or count_empty then
        c = c + 1
      end
      -- todo skip comments
    end
    f:close()
    return { fname, c }
  else
    return { fname, 'error:' .. v2s(err) }
  end
end

---@param data table
function M.mkLocReport(data, opts)
  local total = 0
  local total_loc_by_ext = {}
  local total_files_by_ext = {}
  local s = ''
  local cwd = (opts or E).cwd or ''
  local brief_report = (opts or E).brief_report

  local ignored = data.__ignored
  data.__ignored = nil -- pull

  table.sort(data, base.tbl_compare_map_by_second_num_elm_desc)

  for _, e in ipairs(data) do
    local path, cnt, err = e[1], tonumber(e[2]), ''
    if path and path ~= '' then
      local ext = get_ext(path)
      local relpath = base.get_relative_path(cwd, path)
      if cnt and cnt > 0 then
        total = total + cnt
        ext = ext or '?'

        total_loc_by_ext[ext] = (total_loc_by_ext[ext] or 0) + cnt
        total_files_by_ext[ext] = (total_files_by_ext[ext] or 0) + 1
        total_files_by_ext['total:'] = (total_files_by_ext['total:'] or 0) + 1
      end
      if not brief_report then
        if not cnt then cnt, err = -1, ': ' .. v2s(e[2]) end
        s = s .. fmt("%8s  %s%s\n", cnt, relpath, err)
      end
    end
  end
  if not brief_report then s = s .. "\n\n" end -- sep

  s = s .. "LoC grouped by extensions:\n"
  s = s .. fmt("%16s  %10s  %10s\n", 'extensions', 'LoC', 'files')

  local flat_totals = base.tbl_map_to_list(total_loc_by_ext)
  local flat_ignored = base.tbl_map_to_list(ignored)

  table.sort(flat_totals, base.tbl_compare_map_by_second_num_elm_desc)
  table.sort(flat_ignored, base.tbl_compare_map_by_second_num_elm_desc)

  flat_totals[#flat_totals + 1] = { 'total:', total }
  -- base.tbl_keys_filter_by_typemerge()

  for _, e in ipairs(flat_totals) do
    local ext, loc = e[1], tonumber(e[2]) or ''
    local files = total_files_by_ext[ext] or '?'
    s = s .. fmt("%16s  %10s  %10s\n", v2s(ext), v2s(loc), v2s(files))
  end

  if #flat_ignored > 0 then
    s = s .. "\n\nFiles in which lines were not counted:\n"

    s = s .. fmt("%16s  %10s  %10s\n", 'extensions', '-', 'files')
    for _, e in ipairs(flat_ignored) do
      local ext, cnt = e[1], tonumber(e[2]) or ''
      s = s .. fmt("%16s  %10s  %10s\n", v2s(ext), '-', v2s(cnt))
    end
  end

  if type(opts.ignored_directories) == 'table' then
    s = s .. "\nIgnored Directories:"
    for dir, _ in pairs(opts.ignored_directories) do
      s = s .. ' ' .. v2s(dir)
    end
    s = s .. "\n"
  end

  return s
end

return M
