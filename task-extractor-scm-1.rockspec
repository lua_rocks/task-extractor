---@diagnostic disable: lowercase-global
package = "task-extractor"
version = "scm-1"

source = {
  url = "git+https://gitlab.com/lua_rocks/task-extractor.git"
}

description = {
  homepage = "https://gitlab.com/lua_rocks/task-extractor",
  license = "",
}

dependencies = {
  'lua >= 5.1',
  'alogger >= 0.5',
  'cmd4lua >= 0.8.0',
  'cli-app-base >= 0.8.4',
  'lhttp >= 0.12',
  'inspect >= 3.1',
  'lua-cjson >= 2.1',
  'luasocket >= 3.1',
  'lua-zlib >= 1.2',
  'gumbo >= 0.5',
  'readline >= 3.3',
  -- 'stub >= 0.8', -- testing
}

build = {
  install = {
    bin = {
      ['task-extractor'] = 'bin/task-extractor'
    }
  },
  type = "builtin",
  modules = {
    ['task-extractor.handler'] = "src/task-extractor/handler.lua",
    ['task-extractor.settings'] = "src/task-extractor/settings.lua",
    ['task-extractor.cache'] = "src/task-extractor/cache.lua",
    ['task-extractor.http'] = "src/task-extractor/http.lua",
    ['task-extractor.html'] = "src/task-extractor/html.lua",
    ['task-extractor.json'] = "src/task-extractor/json.lua",
    ['task-extractor.api.sql_academy'] = "src/task-extractor/api/sql_academy.lua",
    ["task-extractor.cmd.cache"] = "src/task-extractor/cmd/cache.lua",
    ["task-extractor.cmd.sql_academy"] = "src/task-extractor/cmd/sql_academy.lua",
    ['task-extractor.api.github'] = "src/task-extractor/api/github.lua",
    ["task-extractor.cmd.github"] = "src/task-extractor/cmd/github.lua",
    ["task-extractor.api.npmjs"] = "src/task-extractor/api/npmjs.lua",
    ["task-extractor.cmd.npmjs"] = "src/task-extractor/cmd/npmjs.lua",
    ["task-extractor.cmd.html"] = "src/task-extractor/cmd/html.lua",
    ['task-extractor.cmd.json'] = "src/task-extractor/cmd/json.lua",
    ['task-extractor.cmd.json_cli'] = "src/task-extractor/cmd/json_cli.lua",
    ['task-extractor.cmd.directory'] = "src/task-extractor/cmd/directory.lua",
    ['task-extractor.cmd.sources'] = "src/task-extractor/cmd/sources.lua",
    ['task-extractor.sources'] = "src/task-extractor/sources.lua",
    ['task-extractor.api.eclipse.jdtls'] = "src/task-extractor/api/eclipse/jdtls.lua",
    ["task-extractor.cmd.eclipse"] = "src/task-extractor/cmd/eclipse.lua",
  }
}
