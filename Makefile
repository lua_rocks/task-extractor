install:
	sudo luarocks make

APP_NAME := "task-extractor"
VERSION := "0.2.0-1"

upload:
	luarocks upload $(APP_NAME)-$(VERSION).rockspec

link:
	ln -s ${PWD}/src/$(APP_NAME)-dev.lua ${HOME}/.local/bin/$(APP_NAME)

unlink:
	unlink ${HOME}/.local/bin/$(APP_NAME)
