## Task-Extractor

CLI tool for obtaining data about tasks from various online services.
To be able to complete training tasks locally and regardless of the Internet
connection.


## Installation

install latest version via luarocks
```sh
luarocks install https://gitlab.com/lua_rocks/task-extractor/-/raw/master/task-extractor-scm-1.rockspec?ref_type=heads
```

## Usage Examples:

- config file
```sh
task-extractor builtin config path
# or
task-extractor b c p

/home/user/.config/task-extractor/config.lua
```

- Count the lines of code in current directory
```sh
task-extractor sources loc
```

TODO:
[x] Cache task data
[ ] Option to work without cache
[ ] Command to cleanup all cache
[ ] Change the order of ddl tables in a ddl schema:
    Issue: In a sql-script for creating database tables that contain descriptions
    of Foreign key constraints, an error will occur if the table to which this
    Foreign Key refers has not yet been created.
[ ] Grap all available tasks
[x] Count lines of code (LoC)
