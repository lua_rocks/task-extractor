-- 06-06-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'task-extractor.cache'

describe("cache", function()

  it("url_to_entry", function()
    local f = M.url_to_entry
    local exp = 'sql-academy.org/ru-trainer-tasks-14'
    assert.same(exp, f('https://sql-academy.org/ru/trainer/tasks/14'))
  end)

end)
