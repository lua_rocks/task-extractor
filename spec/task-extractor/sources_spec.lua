-- 06-08-2024 @author Swarg
require("busted.runner")()
local assert = require 'luassert'

local base = require 'cli-app-base'
local M = require 'task-extractor.sources'

local res_root = (os.getenv('PWD') or '.') .. '/spec/resources/sources/'
local get_res_path = base.build_fn_get_resource_path(res_root)

describe("task-extractor.cmd.sources", function()
  it("calculateLoC", function()
    local opts = {}
    local path = get_res_path('templ.html')
    local t = M.calculateLoC(path, opts)
    local exp = { path, 11 }
    assert.same(exp, t)
  end)
end)
