-- 09-06-2024 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
local M = require 'task-extractor.json'

describe("task-extractor.json", function()
  it("compare_two_table", function()
    local f = M.compare_two_table
    local t1 = { a = 4, b = 5, c = 6 }
    local t2 = { a = 4, b = 5, c = 6 }
    local exp = { true, "a  a\nb  b\nc  c\n" }
    assert.same(exp, { f(t1, t2, '', '') })
  end)

  it("compare_two_table", function()
    local f = M.compare_two_table
    local t1 = { key_a = 4, key_b = 5, key_c = 6 }
    local t2 = { key_a = 4, key_b = 5, key_X = 6 }
    local exp = {
      true,
      "key_a  key_a\nkey_b  key_b\n------------\nkey_c       \n       key_X\n"
    }
    assert.same(exp, { f(t1, t2, '', '') })
  end)

  it("compare_two_table path", function()
    local f = M.compare_two_table
    local t1 = { sub = { key_a = 4, key_b = 5, key_c = 6 } }
    local t2 = { key_a = 4, key_b = 5, key_c = 6 }
    local exp = {
      true,
      "------------\nsub         \n       key_a\n       key_b\n       key_c\n"
    }
    assert.same(exp, { f(t1, t2, '', '') })

    local exp2 = { true, "key_a  key_a\nkey_b  key_b\nkey_c  key_c\n" }
    assert.same(exp2, { f(t1, t2, 'sub', '') })
  end)

  it("compare_two_table path", function()
    local f = M.compare_two_table
    local t1 = { sub = { key_a = 4, key_b = 5, key_c = 6 } }
    local t2 = { key_a = 4, key_b = 5, key_X = 6 }
    local exp = {
      true,
      "------------\nsub         \n       key_X\n       key_a\n       key_b\n"
    }
    assert.same(exp, { f(t1, t2, '', '') })

    local exp2 = {
      true,
      "key_a  key_a\nkey_b  key_b\n------------\nkey_c       \n       key_X\n"
    }
    assert.same(exp2, { f(t1, t2, 'sub', '') })
  end)
end)
