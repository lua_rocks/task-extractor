-- 06-06-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'task-extractor.api.sql_academy'

local base = require 'cli-app-base'
local cjson = require 'cjson'

local E = {}
local cwd = os.getenv('PWD')
local res_root = (cwd or '.') .. '/spec/resources/'

-- obtaine data
-- curl https://sql-academy.org/en/trainer/task/14 -o spec/resources/sql-academy.org/en-trainer-tasks-14.html

local function get_resource(fn)
  local path = res_root .. fn
  return base.read_all(path)
end

describe("sql_academy", function()
  local database_obj = nil

  it("parse_task_response", function()
    local raw = get_resource("sql-academy.org/en-trainer-tasks-14.html")
    local ok, t = M.parse_trainer_task_response(raw)
    assert.same(true, ok)
    assert.is_table(t) ---@cast t table
    assert.same("/trainer/tasks/[taskId]", t.page)
    assert.same('14', t.query.taskId)
    assert.is_table(t.props)
    local st = (((t.props or E).pageProps or E).initialReduxState)
    assert.is_table(st)
    local db = st.trainer.questions.questions[1].database
    database_obj = db
    assert.same('Air travels', db.title)
    assert.same('4ed2b809d7446b9a0e100001', db.id)
    local exp = {
      company = { x = 3, y = 490 },
      pass_in_trip = { x = 380, y = 150 },
      passenger = { x = 387, y = 391 },
      trip = { x = 0, y = 181 }
    }
    assert.same(exp, db.tablePosition)

    local ctx = M.get_page_ctx_from_trainer_task_data(t)
    local exp2 = { page = '/trainer/tasks/14', taskId = '14' }
    -- note addr was: /ru/trainer/tasks/14
    assert.same(exp2, ctx)
  end)

  it("mk_cache_key", function()
    local f = M.mk_cache_key
    local ctx = { page = '/trainer/tasks/14', taskId = '14' }
    assert.same('sql-academy.org/-trainer-tasks-14-db-id', f('-db-id', ctx))
    assert.same('sql-academy.org/1234', f('1234'))
  end)

  local api_resp_1 =
  {
    fields = { "id", "company", "plane", "town_from", "town_to", "time_out", "time_in" },
    rows = { {
      id = 1100,
      company = 4,
      plane = "Boeing",
      time_in = "1900-01-01T17:50:00.000Z",
      time_out = "1900-01-01T14:30:00.000Z",
      town_from = "Rostov",
      town_to = "Paris"
    }, {
      id = 1101,
      company = 4,
      plane = "Boeing",
      time_in = "1900-01-01T11:45:00.000Z",
      time_out = "1900-01-01T08:12:00.000Z",
      town_from = "Paris",
      town_to = "Rostov"
    }
    }
  }

  it("mk_sql_dml_insert_values", function()
    local f = M.mk_sql_dml_insert_values
    local exp_dml = [[
INSERT INTO Trip (id, company, plane, town_from, town_to, time_out, time_in)
VALUES
(1100, 4, Boeing, Rostov, Paris, 1900-01-01T14:30:00.000Z, 1900-01-01T17:50:00.000Z),
(1101, 4, Boeing, Paris, Rostov, 1900-01-01T08:12:00.000Z, 1900-01-01T11:45:00.000Z)
;]]
    assert.same(exp_dml, f('Trip', api_resp_1))
  end)

  it("mk_sql_ddl_create_table", function()
    local f = M.mk_sql_ddl_create_table
    local tbl_scheme = M.get_table_data(database_obj, 'Trip')
    local exp_ddl = [[
CREATE TABLE IF NOT EXISTS Trip (
  id INT PRIMARY KEY,
  company INT,         -- Name of the carrier company
  plane VARCHAR,       -- Aircraft model
  town_from VARCHAR,   -- Departure city
  town_to VARCHAR,     -- Arrival city
  time_out DATETIME,   -- Departure time
  time_in DATETIME     -- Arrival time
);]]
    assert.same(exp_ddl, f('Trip', tbl_scheme))
  end)

  it("get_table_data", function()
    local f = M.get_table_data
    local exp = {
      description = 'Air travel companies',
      id = 'company',
      props = {
        {
          description = cjson.null,
          isKey = true,
          name = 'id',
          type = 'INT',
          id = 'id',
        },
        {
          description = 'Name of the carrier company',
          id = 'name',
          isKey = false,
          name = 'name',
          type = 'VARCHAR'
        }
      },
      title = 'Company'
    }
    assert.same(exp, f(database_obj, 'Company'))
  end)

  it("get_column_pros", function()
    local table_schema = M.get_table_data(database_obj, 'Company')
    local f = M.get_column_pros
    local exp = {
      description = 'Name of the carrier company',
      id = 'name',
      isKey = false,
      name = 'name',
      type = 'VARCHAR'
    }
    assert.same(exp, f(table_schema, 'name'))
    local exp2 = {
      description = cjson.null,
      id = 'id',
      isKey = true,
      name = 'id',
      type = 'INT',
    }
    assert.same(exp2, f(table_schema, 'id'))
  end)

  it("get_tables_relations", function()
    local f = M.get_tables_relations
    local exp = {}
    assert.same(exp, f(database_obj, 'Company'))
    local exp2 = {
      passenger = {
        to_table = 'passenger',
        to_column = 'id',
        type = 'many-to-one'
      },
      trip = {
        to_table = 'trip',
        to_column = 'id',
        type = 'many-to-one'
      }
    }
    assert.same(exp2, f(database_obj, 'pass_in_trip'))

    local exp3 = {}
    assert.same(exp3, f(database_obj, 'passenger'))
  end)

  it("mk_sql_ddl_create_table", function()
    local relations = M.get_tables_relations(database_obj, 'Trip')
    local f = M.mk_sql_ddl_create_table
    local tbl_scheme = M.get_table_data(database_obj, 'Trip')
    local exp_ddl = [[
CREATE TABLE IF NOT EXISTS Trip (
  id INT PRIMARY KEY,
  company INT,         -- Name of the carrier company
  plane VARCHAR,       -- Aircraft model
  town_from VARCHAR,   -- Departure city
  town_to VARCHAR,     -- Arrival city
  time_out DATETIME,   -- Departure time
  time_in DATETIME     -- Arrival time

  FOREIGN KEY (company) REFERENCES company(id)
);]]
    assert.same(exp_ddl, f('Trip', tbl_scheme, relations))
  end)

  it("get_coltypes_for_table", function()
    local f = M.get_coltypes_for_table
    local exp = {
      id = 'INT',
      company = 'INT',
      plane = 'VARCHAR',
      time_in = 'DATETIME',
      time_out = 'DATETIME',
      town_from = 'VARCHAR',
      town_to = 'VARCHAR'
    }
    assert.same(exp, f(database_obj, 'Trip'))
  end)

  local question_id1 =
  {
    _id = "4ed2b809d7446b9a0e000001",
    id = 1,
    author = nil,
    difficulty = "easy",
    premiumOnly = false,
    database = {
      title = "Air travels"
    },
    isSolved = false,
    isRevealed = false,
    isSolvedWithRevealing = false,
    question = "Print the names of all the people who are in the airline database"
  }
  it("format_question", function()
    local f = M.format_question
    local exp = [[
#1 [easy] db:'Air travels' :
Print the names of all the people who are in the airline database
]]
    assert.same(exp, f(question_id1, {}))
  end)

  it("get_table_names_n_desc", function()
    local f = M.get_table_names_n_desc
    assert.is_table(database_obj) ---@cast database_obj table

    local exp = {
      { 'company', 'passenger', 'pass_in_trip', 'trip' },
      {
        'Air travel companies',
        'Passengers who bought a ticket',
        'List of purchased tickets',
        'Flight schedule'
      }
    }
    assert.same(exp, { f(database_obj) })
  end)
end)
