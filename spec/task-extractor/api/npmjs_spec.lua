-- 03-09-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local M = require 'task-extractor.api.npmjs'

describe("task-extractor.api.npmjs", function()
  it("getLastPublishFromOpInternal", function()
    local f = M.getLastPublishFromOpInternal
    local t = {
      _npmOperationalInternal = {
        tmp = "tmp/abc_1.2.3_1724429433263_0.34211381570954136",
        host = "s3://npm-registry-packages"
      }
    }
    assert.same({ '2024-08-23', 'abc', '1.2.3' }, { f(t) })
  end)

  it("readable_keywords", function()
    local f = M.readable_keywords
    local t = { "abc", "de f", "j h k" }
    assert.same('abc de-f j-h-k', f(t))
  end)


  it("fmt_comparision_table_markdown", function()
    local t = {
      {
        ['package'] = 4,
        downloads = 5,
        repository = 12,
        stars = 5,
        forks = 2,
        created_at = 7,
        last_commit = 7,
      },
      {
        ['package'] = "liba",
        author = "author1",
        maintainers = '2: author1 another-man',
        created_at = "2024-09",
        description = "desc1",
        downloads = "1.0M",
        topics = "key1 key2",
        homepage = "https://github.com/user/liba#readme",
        last_commit = "2024-09",
        repository = "user/liba",
        repourl = "https://github.com/user/liba",
        forks = 23,
        stars = 123,
      },
      {
        ['package'] = "libb",
        author = "author2",
        maintainers = '1: author2',
        created_at = "2023-09",
        description = "desc2",
        downloads = "10.3K",
        topics = "key3 key4",
        homepage = "https://github.com/author2/libb#readme",
        last_commit = "2023-05",
        repository = "author2/libb",
        repourl = "https://github.com/author2/libb",
        forks = 3,
        stars = 23,
      }
    }
    local exp = [[
| pkg  | dwlds |  repository  | stars |  f | lcommit | created |
| ---- | ----- | ------------ | ----- | -- | ------- | ------- |
| liba |  1.0M | user/liba    |   123 | 23 | 2024-09 | 2024-09 |
| libb | 10.3K | author2/libb |    23 |  3 | 2023-05 | 2023-09 |

- liba https://github.com/user/liba#readme
(2024-09) https://github.com/user/liba
author: author1  2: author1 another-man
descrp: desc1
topics: key1 key2

- libb https://github.com/author2/libb#readme
(2023-09) https://github.com/author2/libb
author: author2  1: author2
descrp: desc2
topics: key3 key4
]]
    assert.same(exp, M.fmt_comparision_table_markdown(t))

    local opts = { minify = true, no_footnotes = true }
    local exp_min = [[
|pkg |dwlds| repository |stars| f|lcommit|created|
|----|-----|------------|-----|--|-------|-------|
|liba| 1.0M|user/liba   |  123|23|2024-09|2024-09|
|libb|10.3K|author2/libb|   23| 3|2023-05|2023-09|
]]
    assert.same(exp_min, M.fmt_comparision_table_markdown(t, opts))

    --
    t[1].stars = 1
    t[1].name = 4
    t[2].stars = 1
    t[3].stars = 2

    local exp_min2 = [[
|pkg |dwlds| repository |*| f|lcommit|created|
|----|-----|------------|-|--|-------|-------|
|liba| 1.0M|user/liba   |1|23|2024-09|2024-09|
|libb|10.3K|author2/libb|2| 3|2023-05|2023-09|
]]
    assert.same(exp_min2, M.fmt_comparision_table_markdown(t, opts))
  end)
end)
