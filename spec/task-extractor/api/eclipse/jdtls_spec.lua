-- 27-06-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")
local gumbo = require "gumbo"
local base = require 'cli-app-base'
local M = require 'task-extractor.api.eclipse.jdtls'

describe("task-extractor.api.eclipse.jdtls", function()
  local html_div_dirlist = [[
<!DOCTYPE html>
<html lang="en">
  <head>
  </head>
  <body>
  <div id='dirlist'>
    <table class='.table' style='width:100%; font-family: monospace;'>
      <tr>
        <td></td>
        <td>
          <img src='folder.png' />
          <a href='/jdtls/milestones/..'> ..</a>
        </td>
        <td style="text-align: right; padding-right:15px;"></td>
        <td></td>
        <td></td>
      </tr>

      <tr>
        <td></td>
        <td>
          <img src='folder.png' />
          <a href='/jdtls/milestones/0.1.0'> 0.1.0</a>
        </td>
        <td style="text-align: right; padding-right:15px;"> 30.2M </td>
        <td>2017-11-02 15:43</td><td></td>
      </tr>

      <tr>
        <td></td>
        <td><img src='folder.png' />
          <a href='/jdtls/milestones/0.10.0'> 0.10.0</a> </td>
          <td style="text-align: right; padding-right:15px;"> 39.2M </td>
          <td>2017-12-15 16:00</td>
          <td></td>
      </tr>
    </table>
  </div>
  </body></hmtl>
  ]]

  -- <div><table>       table in childNodes[1]
  -- <div>\n <table>  - table in childNodes[2]
  it("find_next_element", function()
    local doc = gumbo.parse(html_div_dirlist)
    local dirlist = doc:getElementById("dirlist")
    assert.same(2, M.find_next_element('table', dirlist))

    doc = gumbo.parse("<div id='dirlist'><table><tr></tr></table></div>")
    dirlist = doc:getElementById("dirlist")
    assert.same(1, M.find_next_element('table', dirlist))

    doc = gumbo.parse("<div id='dirlist'>  <table><tr></tr></table></div>")
    dirlist = doc:getElementById("dirlist")
    assert.same(2, M.find_next_element('table', dirlist))
  end)

  local function slim(s)
    local res = ''
    for _, line in ipairs(base.str_split(s, "\n")) do
      res = res .. base.str_trim(line)
    end
    return res
  end

  it("slim", function()
    assert.same('somethingwith spaces', slim("some\n  thing \n with spaces"))
  end)

  it("html_tbl_to_obj", function()
    local html = slim(html_div_dirlist)
    local doc = gumbo.parse(html)
    local dirlist = doc:getElementById("dirlist")
    local i = M.find_next_element('table', dirlist)
    assert.same(1, i)
    assert.is_table(dirlist.childNodes[i])
    local exp = {
      { '/jdtls/milestones/../' },
      { '/jdtls/milestones/0.1.0/',  ' 30.2M ', '2017-11-02 15:43' },
      { '/jdtls/milestones/0.10.0/', ' 39.2M ', '2017-12-15 16:00' }
    }
    assert.same(exp, M.html_tbl_to_obj(dirlist.childNodes[i]))
  end)
end)
