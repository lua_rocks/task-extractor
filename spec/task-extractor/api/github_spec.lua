-- 08-06-2024 @author Swarg
--
-- how I got the test data:
-- curl https://github.com/lunarmodules/luasql/tree/master/doc -o spec/resources/github/luasql-repo-doc.html
-- curl https://github.com/lunarmodules/luasql -o spec/resources/github/luasql-repo.html
-- task-extractor html slim spec/resources/github/luasql-repo.html -F
-- task-extractor html slim spec/resources/github/luasql-doc.html -F
--
require 'busted.runner' ()
local assert = require 'luassert'
local M = require 'task-extractor.api.github'

local base = require 'cli-app-base'
local cjson = require 'cjson'

local NULL = cjson.null

local res_root = (os.getenv('PWD') or '.') .. '/spec/resources/github/'
local get_resource = base.build_fn_get_resource(res_root)

describe("task-extractor.api.github luasql-repo", function()
  local exp_repo_info = {
    id = 315988,
    createdAt = '2009-09-24T01:55:33.000Z',
    currentUserCanPush = false,
    defaultBranch = 'master',
    isEmpty = false,
    isFork = false,
    isOrgOwned = true,
    name = 'luasql',
    ownerAvatar = 'https://avatars.githubusercontent.com/u/70332310?v=4',
    ownerLogin = 'lunarmodules',
    private = false,
    public = true
  }
  -- subdir in repo
  it("get_code_view_data", function()
    local html = get_resource('luasql-repo.html.1')
    assert.is_string(html)
    local ok, res = M.get_repos_overview_data(html)
    if not ok then print(res) end
    assert.same(true, ok)
    assert.is_table(res) ---@cast res table
    local p = res.props.initialPayload
    assert.is_table(p) ---@cast p table

    local exp2 = exp_repo_info
    assert.same(exp2, p.repo)
  end)

  -- subdir in repo
  it("get_code_view_data", function()
    local page = get_resource('luasql-doc.html.1')
    assert.is_string(page)
    local ok, res = M.get_code_view_data(page)
    assert.same(true, ok)
    assert.is_table(res) ---@cast res table
    local tree = res.payload.tree
    if tree.readme and tree.readme ~= NULL then
      tree.readme.richText = 'SKIPPED'
    end

    local exp = {
      items = {
        { contentType = "directory", name = "br", path = "doc/br" },
        { contentType = "directory", name = "fr", path = "doc/fr" },
        { contentType = "directory", name = "us", path = "doc/us" }
      },

      readme = NULL,
      showBranchInfobar = false,
      templateDirectorySuggestionUrl = NULL,
      totalCount = 3
    }

    assert.same(exp, tree)
    local exp2 = exp_repo_info
    assert.same(exp2, res.payload.repo)
  end)

  it("parse_github_url", function()
    local f = M.parse_github_url -- to user, repo, branch, path
    assert.same({ 'user', 'repo' }, { f('https://github.com/user/repo') })
    assert.same({ 'ac_c', 'rp' }, { f('https://github.com/ac_c/rp') })
    assert.same({ 'ac', 'rp', 'dev', 'sub' }, { f('https://github.com/ac/rp/x/dev/sub') })
  end)
  it("parse_github_url", function()
    local f = M.parse_github_url
    local url = 'https://github.com/user/repo/tree/master/path/in/project'
    local exp = { 'user', 'repo', 'master', 'path/in/project' }
    assert.same(exp, { f(url) })
  end)

  it("get_url_to_raw_file", function()
    local f = M.get_url_to_raw_file
    local exp = 'https://raw.githubusercontent.com/user/repo/master/path'
    assert.same(exp, f('user', 'repo', 'path', 'master'))
  end)

  it("mk_full_repo_url", function()
    local f = M.mk_full_repo_url
    local exp_ok = { true, 'https://github.com/org/repo' }
    assert.same(exp_ok, { f("org/repo") })
    assert.same(exp_ok, { f("https://github.com/org/repo") })
    assert.same(exp_ok, { f("git+https://github.com/org/repo.git") })
    assert.same(exp_ok, { f("git://github.com/org/repo.git") })

    local exp_err = { false, 'expected at least user/repo' }
    assert.same(exp_err, { f("org") })
    assert.same(exp_err, { f("") })
    assert.same(exp_err, { f(nil) })
  end)
end)
