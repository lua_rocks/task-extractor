-- 07-06-2024 @author Swarg
require("busted.runner")()
local assert = require 'luassert'
local M = require 'task-extractor.http'

describe("html", function()
  it("get_tag_body_range", function()
    local f = M.get_tag_body_range

    -- require 'alogger'.fast_setup(nil, 0)
    local exp = { 24, 27 }
    local html = '<script attr=abc more=>body</script>'
    assert.same(exp, { f(html) })
    assert.same('body', html:sub(exp[1], exp[2]))

    html = '<script attr=abc more=>body</script>'
        .. '<script attr="x" more=>BODY</script>'
    local exp2 = { 60, 63 }
    assert.same(exp2, { f(html, '<script', '</script>', 'attr="x"', 1, #html) })
    assert.same('BODY', html:sub(exp2[1], exp2[2]))
  end)

  local html_with_scripts = '<html><body>'
      .. '<script attr=abc more=>body</script>'
      .. ' some text 1'
      .. '<script attr="x" more=>BODY</script>'
      .. ' some text 2'
      .. '<script attr="x" more=>BODY</script>'
      .. ' some text 3</body></html>'

  it("find_all_tags", function()
    local f = M.find_all_tags
    local otag, ctag, attr = '<script', '</script>', nil
    local exp = { { 36, 39 }, { 84, 87 }, { 132, 135 } }
    assert.same(exp, f(html_with_scripts, otag, ctag, attr))
  end)

  it("get_readable_tag_ocurrences", function()
    local f = M.get_readable_tag_ocurrences
    local ranges = { { 36, 39 }, { 84, 87 }, { 132, 135 } }
    assert.same("36-39\n84-87\n132-135\n", f(html_with_scripts, ranges))

    local opts = { verbose = true }
    local exp = "36-39 body\n84-87 BODY\n132-135 BODY\nTotal: 3"
    assert.same(exp, f(html_with_scripts, ranges, opts))
  end)
end)
