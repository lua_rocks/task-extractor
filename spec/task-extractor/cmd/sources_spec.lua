-- 06-08-2024 @author Swarg
require("busted.runner")()
local assert = require 'luassert'

local Cmd4Lua = require 'cmd4lua'
local M = require 'task-extractor.cmd.sources'


describe("task-extractor.cmd.sources", function()
  it("cmd_lines_of_code", function()
    local st = {}
    local f = function(t, s) t[#t + 1] = s end

    local w = Cmd4Lua.of('./spec/resources/sources/'):set_print_callback(f, st)

    M.cmd_lines_of_code(w)

    local exp = [[
      11  ./spec/resources/sources/templ.html


LoC grouped by extensions:
      extensions         LoC       files
            html          11           1
          total:          11           1

Ignored Directories: .git vendor
]]
    assert.same(exp, st[1])
  end)
end)
