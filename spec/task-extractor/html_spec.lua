-- 08-06-2024 @author Swarg

require 'busted.runner' ()
local assert = require 'luassert'

local gumbo = require 'gumbo'
local base = require 'cli-app-base'


local res_root = (os.getenv('PWD') or '.') .. '/spec/resources/github/'
local get_resource = base.build_fn_get_resource(res_root)

describe("task-extractor.html", function()
  it("gumbo richText", function()
    local s = [[
<div class="plain"><pre style="white-space: pre-wrap">LuaSQL
<a href="https://lunarmodules.github.io/luasql/"
rel="nofollow">https://lunarmodules.github.io/luasql/</a>

luaSQL is a simple interface from Lua to a DBMS. It enables a Lua program to:

    * Connect to ODBC, ADO, Oracle, MySQL, SQLite, Firebird and PostgreSQL databases;
    * Execute arbitrary SQL statements;
    * Retrieve results in a row-by-row cursor fashion.

LuaSQL is free software and uses the same license as Lua 5.1.


Source code for LuaSQL can be downloaded from its GitHub repository.

</pre></div>
  ]]

    local document = assert(gumbo.parse(s))
    local exp = [[
LuaSQL
https://lunarmodules.github.io/luasql/

luaSQL is a simple interface from Lua to a DBMS. It enables a Lua program to:

    * Connect to ODBC, ADO, Oracle, MySQL, SQLite, Firebird and PostgreSQL databases;
    * Execute arbitrary SQL statements;
    * Retrieve results in a row-by-row cursor fashion.

LuaSQL is free software and uses the same license as Lua 5.1.


Source code for LuaSQL can be downloaded from its GitHub repository.


  ]]
    assert.same(exp, document.body.textContent)
  end)
end)
